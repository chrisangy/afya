<?php

namespace App\Entity\AppBundle;

use Doctrine\ORM\Mapping as ORM;

/**
 * Analyse
 *
 * @ORM\Table(name="analyse")
 * @ORM\Entity(repositoryClass=App\Repository\AppBundle\AnalyseRepository")
 */
class Analyse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\Dossier")
     * @ORM\JoinColumn(nullable=false)
     */
    private $dossier;

//    /**
//     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Patient")
//     * @ORM\JoinColumn(nullable=false)
//     */
//    private $patient;

    /**
     * @todo : remettre a false apres les test
     * @ORM\ManyToOne(targetEntity=App\Entity\UserBundle\Laborantin")
     * @ORM\JoinColumn(nullable=true)
     */
    private $laborantin;


    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text")
     */
    private $note;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;

    public function __construct()
    {

        $this->createdAt = new \DateTime();
        $this->deleted = false;
    }


}
