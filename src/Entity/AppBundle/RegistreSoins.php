<?php

namespace App\Entity\AppBundle;

use AppBundle\TraitClass\EntityDateTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * RegistreSoins
 *
 * @ORM\Table(name="registre_soins")
 * @ORM\Entity(repositoryClass=App\Repository\AppBundle\RegistreSoinsRepository")
 */
class RegistreSoins
{

    use EntityDateTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\UserBundle\Patient")
     * @ORM\JoinColumn(nullable=true)
     */
    private $patient;

    private $typePatient;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateSoins", type="datetime")
     */
    private $dateSoins;

    /**
     * @var string
     *
     * @ORM\Column(name="temperature", type="decimal", precision=10, scale=2)
     */
    private $temperature;

    /**
     * @var string
     *
     * @ORM\Column(name="poids", type="decimal", precision=10, scale=2)
     */
    private $poids;

    /**
     * @var string
     *
     * @ORM\Column(name="taille", type="decimal", precision=10, scale=2,nullable=true)
     */
    private $taille;

    /**
     * @var string
     *
     * @ORM\Column(name="age", type="float",nullable=true)
     */
    private $age;


    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string",nullable=true)
     */
    private $sexe;


    /**
     * @var string
     *
     * @ORM\Column(name="symptome", type="text")
     */
    private $symptome;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\Affections")
     * @ORM\JoinColumn(nullable=true)
     */
    private $diagnostique;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\Bilan")
     * @ORM\JoinColumn(nullable=true)
     */
    private $bilanDemande;

    /**
     * @var string
     *
     * @ORM\Column(name="prescription", type="text",nullable=true)
     */
    private $prescription;

    /**
     * @var string
     *
     * @ORM\Column(name="posologie", type="text",nullable=true)
     */
    private $posologie;
    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text",nullable=true)
     */
    private $observation;
    /**
     * @var string
     *
     * @ORM\Column(name="estMort", type="boolean")
     */
    private $estMort;
    /**
     * @var string
     *
     * @ORM\Column(name="estHospitalise", type="boolean")
     */
    private $estHospitalise;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->deleted = false;
        $this->dateSoins = new \DateTime();
    }


}
