<?php

namespace App\Entity\AppBundle;

use AppBundle\TraitClass\EntityDateTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Dettes
 *
 * @ORM\Table(name="dettes")
 * @ORM\Entity(repositoryClass=App\Repository\AppBundle\DettesRepository")
 */
class Dettes
{

    use EntityDateTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="montant", type="decimal", precision=10, scale=2)
     */
    private $montant;


    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\CompteDebiteur")
     * @ORM\JoinColumn(nullable=true)
     */
    private $compte;



}
