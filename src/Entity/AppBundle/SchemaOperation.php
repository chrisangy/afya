<?php

namespace App\Entity\AppBundle;

use Doctrine\ORM\Mapping as ORM;

/**
 * SchemaOperation
 *
 * @ORM\Table(name="schema_operation")
 * @ORM\Entity(repositoryClass=App\Repository\AppBundle\SchemaOperationRepository")
 */
class SchemaOperation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;


    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\TypeProduit")
     * @ORM\JoinColumn(nullable=true)
     */
    private $typeProduit;

    public function __construct()
    {

        $this->created = new \DateTime();
        $this->deleted = false;
        $this->code= 'AFSH-'.substr(str_shuffle('0123456789'), 0, 5);
    }


}
