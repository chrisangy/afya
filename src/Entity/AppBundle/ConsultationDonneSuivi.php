<?php

namespace App\Entity\AppBundle;

use AppBundle\TraitClass\EntityDateTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * ConsultationDonneSuivi
 *
 * @ORM\Table(name="consultation_donne_suivi")
 * @ORM\Entity(repositoryClass=App\Repository\AppBundle\ConsultationDonneSuiviRepository")
 */
class ConsultationDonneSuivi
{
    use EntityDateTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\Consultation")
     * @ORM\JoinColumn(nullable=false)
     */
    private $consultation;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\DonneSuivi")
     * @ORM\JoinColumn(nullable=false)
     */
    private $donnesuivi;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, nullable=true)
     */
    private $reference;

    public function __construct()
    {
        $this->createdAt  =  new \DateTime();
        $this->deleted  =  false;
        $this->reference = 'CNDNSU-00' . substr(mt_rand(), 0, 5);

    }


}
