<?php

namespace App\Entity\AppBundle;

use Doctrine\ORM\Mapping as ORM;

/**
 * CompteDebiteur
 *
 * @ORM\Table(name="compte_debiteur")
 * @ORM\Entity(repositoryClass=App\Repository\AppBundle\CompteDebiteurRepository")
 */
class CompteDebiteur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\Dossier")
     * @ORM\JoinColumn(nullable=false)
     */
    private $dossier;




}
