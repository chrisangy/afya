<?php

namespace App\Entity\AppBundle;

use Doctrine\ORM\Mapping as ORM;

/**
 * AnalyseImage
 *
 * @ORM\Table(name="analyse_image")
 * @ORM\Entity(repositoryClass=App\Repository\AppBundle\AnalyseImageRepository")
 */
class AnalyseImage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\Analyse")
     * @ORM\JoinColumn(nullable=false)
     */
    private $analyse;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\Image")
     * @ORM\JoinColumn(nullable=false)
     */
    private $image;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;


}
