<?php

namespace App\Entity\AppBundle;

use Doctrine\ORM\Mapping as ORM;

/**
 * Consultation
 *
 * @ORM\Table(name="consultation")
 * @ORM\Entity(repositoryClass=App\Repository\AppBundle\ConsultationRepository")
 */
class Consultation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\Dossier")
     * @ORM\JoinColumn(nullable=false)
     */
    private $dossier;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateConsultation", type="datetime")
     */
    private $dateConsultation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var bool
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;

    /**
     * @var string
     *
     * @ORM\Column(name="motif", type="text")
     */
    private $motif;
    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, nullable=true)
     */
    private $reference;

    public function __construct()
    {
        $this->reference = 'CON-00' . substr(mt_rand(), 0, 5);
        $this->createdAt = new \DateTime();
        $this->deleted = false;

    }


}
