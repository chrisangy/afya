<?php

namespace App\Entity\AppBundle;

use Doctrine\ORM\Mapping as ORM;

/**
 * RendezVous
 *
 * @ORM\Table(name="rendez_vous")
 * @ORM\Entity(repositoryClass=App\Repository\AppBundle\RendezVousRepository")
 */
class RendezVous
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\UserBundle\Patient")
     * @ORM\JoinColumn(nullable=true)
     */
    private $patient;


    private $typeClient;


    /**
     *  
     * @ORM\ManyToOne(targetEntity=App\Entity\UserBundle\Medecin")
     * @ORM\JoinColumn(nullable=false)
     */
    private $medecin;

    /**
     * @var string
     *
     * @ORM\Column(name="motif", type="text")
     */
    private $motif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDemande", type="datetime",nullable=true)
     */
    private $dateDemande;

    /**
     * @var string
     *
     * @ORM\Column(name="jour", type="string", length=20,nullable=true)
     */
    private $jour;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heure", type="time",nullable=true)
     */
    private $heure;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estValider", type="boolean")
     */
    private $estValider;

    /**
     * @var bool
     *
     * @ORM\Column(name="estConsumer", type="boolean")
     */
    private $estConsumer;

    /**
     * @var bool
     *
     * @ORM\Column(name="estAnnuler", type="boolean")
     */
    private $estAnnuler;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;

    public function __construct(){
        $this->deleted= false;
        $this->estValider= false;
        $this->estConsumer= false;
        $this->estAnnuler= false;
        $this->createdAt  =  new \DateTime();
        $date  = new \Datetime();

        $unixTimestamp = strtotime($date->format('d-m-Y'));
        $unixTime = strtotime($date->format('H:i'));
        $this->jour ="fdd";

    }


}
