<?php

namespace App\Entity\AppBundle;

use AppBundle\TraitClass\EntityDateTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * TypeDonne
 *
 * @ORM\Table(name="type_donne")
 * @ORM\Entity(repositoryClass=App\Repository\AppBundle\TypeDonneRepository")
 */
class TypeDonne
{
    use EntityDateTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", unique=true, length=255)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, nullable=true)
     */
    private $reference;
    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\TypeDonne")
     * @ORM\JoinColumn(nullable=true)
     */
    private $parent;
    public function __construct()
    {
        $this->reference = 'TYPDN-00' . substr(mt_rand(), 0, 5);

    }
    public function __toString()
    {
        return (string)$this->libelle;
    }



}
