<?php

namespace App\Entity\AppBundle;

use AppBundle\TraitClass\EntityDateTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * DonneSuivi
 *
 * @ORM\Table(name="donne_suivi")
 * @ORM\Entity(repositoryClass=App\Repository\AppBundle\DonneSuiviRepository")
 */
class DonneSuivi
{
    use EntityDateTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, unique=true, nullable=true)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\TypeDonne")
     * @ORM\JoinColumn(nullable=true)
     */
    private $typeDonne;
    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\TypeDonne")
     * @ORM\JoinColumn(nullable=true)
     */
    private $subtypeDonne;
    public function __construct()
    {
        $this->reference = 'DNSU-00' . substr(mt_rand(), 0, 5);

    }

}
