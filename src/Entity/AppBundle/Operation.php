<?php

namespace App\Entity\AppBundle;

use AppBundle\TraitClass\TypePatientTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Operation
 *
 * @ORM\Table(name="operation")
 * @ORM\Entity(repositoryClass=App\Repository\AppBundle\OperationRepository")
 */
class Operation
{

    use TypePatientTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="reference", type="string")
     */
    private $reference;
    /**
     * @var string
     *
     * @ORM\Column(name="nimMECeF", type="string",nullable=true)
     */
    private $nimMECeF;
    /**
     * @var string
     *
     * @ORM\Column(name="countersMECeF", type="string",nullable=true)
     */
    private $countersMECeF;
    /**
     * @var string
     *
     * @ORM\Column(name="codeMECeFDGI", type="string",nullable=true)
     */
    private $codeMECeFDGI;
    /**
     * @var string
     *
     * @ORM\Column(name="qrCodeMECeFDGI", type="text",nullable=true)
     */
    private $qrCodeMECeFDGI;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateOperation", type="datetime")
     */
    private $dateOperation;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\StockBundle\CategorieArticle")
     * @ORM\JoinColumn(nullable=true)
     */
    private $categorieArticle;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\StockBundle\Article")
     * @ORM\JoinColumn(nullable=true)
     */
    private $produit;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="integer")
     */
    private $nombre = 1;
    /**
     * @var string
     *
     * @ORM\Column(name="montant", type="integer")
     */
    private $montant = 0;
    /**
     * @var string
     *
     * @ORM\Column(name="prix", type="integer",nullable=true)
     */
    private $prix = 0;
    /**
     * @var string
     *
     * @ORM\Column(name="montantRemisParClient", type="decimal")
     */
    private $montantRemisParClient = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="monnaieRendu", type="integer")
     */
    private $monnaieRendu = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleOperation", type="string", length=255)
     */
    private $libelleOperation;


    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\UserBundle\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;


    /**
     * todo : mettre la table generale patient ici et vois la faisabilite de form event !!!!!
     * @ORM\ManyToOne(targetEntity=App\Entity\UserBundle\Personne")
     * @ORM\JoinColumn(nullable=true)
     */
    private $patient;


    /**
     *
     */
    private $compteDette;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;
    /**
     * @var string
     *
     * @ORM\Column(name="estDette", type="boolean")
     */
    private $estDette = false;


    public function __construct()
    {
        $date = new \DateTime();
        $this->created = new \DateTime();
        $this->dateOperation = $date;
        $this->deleted = false;
        $this->libelleOperation = '';
    }


}
