<?php

namespace App\Entity\AppBundle;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrdonanceMedicament
 *
 * @ORM\Table(name="ordonance_medicament")
 * @ORM\Entity(repositoryClass=App\Repository\AppBundle\OrdonanceMedicamentRepository")
 */
class OrdonanceMedicament
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string",length =50)
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\Consultation")
     * @ORM\JoinColumn(nullable=true)
     */
    private $consultation;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity=App\Entity\UserBundle\Patient")
     * @ORM\JoinColumn(nullable=true)
     */
    private $patient;

    /**
     * @var string
     *
     * @ORM\Column(name="medicament", type="string", length=255)
     */
    private $medicament;

    /**
     * @var int
     *
     * @ORM\Column(name="nombre", type="integer")
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="unite", type="string", length=255)
     */
    private $unite;

    /**
     * @var string
     *
     * @ORM\Column(name="posologie", type="string", length=255)
     */
    private $posologie;


    /**
     * @var bool
     *
     * @ORM\Column(name="estEditerApresConsultation", type="boolean")
     */
    private $estEditerApresConsultation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEdition", type="datetime")
     */
    private $dateEdition;

    /**
     * @var bool
     *
     * @ORM\Column(name="supprimer", type="boolean")
     */
    private $supprimer;



}
