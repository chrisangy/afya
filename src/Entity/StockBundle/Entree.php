<?php

namespace App\Entity\StockBundle;

use AppBundle\TraitClass\EntityDateTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Entree
 *
 * @ORM\Table(name="entree")
 * @ORM\Entity(repositoryClass=App\Repository\StockBundle\EntreeRepository")
 */
class Entree
{

    use EntityDateTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, unique=true)
     */
    private $reference;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="integer")
     */
    private $quantite;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\StockBundle\Article", inversedBy="entree")
     * @ORM\JoinColumn(nullable=false)
     */
    private $article;
    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\StockBundle\Fournisseur")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fournisseur;


}
