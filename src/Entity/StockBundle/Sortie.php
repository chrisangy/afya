<?php

namespace App\Entity\StockBundle;

use AppBundle\TraitClass\EntityDateTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\StockBundle\SortieRepository")
 */
class Sortie
{

    use EntityDateTrait;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantite;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\StockBundle\Article", inversedBy="sorties")
     * @ORM\JoinColumn(nullable=false)
     */
    private $article;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\StockBundle\Departement")
     * @ORM\JoinColumn(nullable=false)
     */
    private $departement;


}
