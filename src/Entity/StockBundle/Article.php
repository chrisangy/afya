<?php

namespace App\Entity\StockBundle;

use AppBundle\TraitClass\EntityDateTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass=App\Repository\StockBundle\ArticleRepository")
 */
class Article
{
    use EntityDateTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, unique=true)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255)
     */
    private $designation;

    /**
     * @var int
     *
     * @ORM\Column(name="qteStock", type="integer")
     */
    private $qteStock = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="prixVente", type="decimal",nullable=true)
     */
    private $prixVente = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="seuil", type="integer", length=255)
     */
    private $seuil = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datePeremption", type="datetime", nullable=true)
     */
    private $datePeremption;

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\StockBundle\Entree", mappedBy="article", orphanRemoval=true)
     */
    private $entrees;

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\StockBundle\Sortie", mappedBy="article", orphanRemoval=true)
     */
    private $sorties;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\StockBundle\CategorieArticle")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorie;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->deleted = false;
        $this->entrees = new ArrayCollection();
        $this->sorties = new ArrayCollection();
    }


    public function __toString()
    {
        return (string)$this->designation;
    }

}
