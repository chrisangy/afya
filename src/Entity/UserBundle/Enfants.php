<?php

namespace App\Entity\UserBundle;

use Doctrine\ORM\Mapping as ORM;

/**
 * Enfants
 *
 * @ORM\Table(name="enfants")
 * @ORM\Entity(repositoryClass=App\Repository\UserBundle\EnfantsRepository")
 */
class Enfants extends Personne
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="poidsNaissance", type="decimal", precision=10, scale=2,nullable=true)
     */
    private $poidsNaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="LA", type="string", length=255,nullable=true)
     */
    private $lA;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heure", type="time",nullable=true)
     */
    private $heure;

    public function __construct()
    {
        parent::__construct();
        $this->poidsNaissance = 0;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set poidsNaissance
     *
     * @param string $poidsNaissance
     *
     * @return Enfants
     */
    public function setPoidsNaissance($poidsNaissance)
    {
        $this->poidsNaissance = $poidsNaissance;

        return $this;
    }

    /**
     * Get poidsNaissance
     *
     * @return string
     */
    public function getPoidsNaissance()
    {
        return $this->poidsNaissance;
    }

    /**
     * Set lA
     *
     * @param string $lA
     *
     * @return Enfants
     */
    public function setLA($lA)
    {
        $this->lA = $lA;

        return $this;
    }

    /**
     * Get lA
     *
     * @return string
     */
    public function getLA()
    {
        return $this->lA;
    }

    /**
     * @return \DateTime
     */
    public function getHeure()
    {
        return $this->heure;
    }

    /**
     * @param \DateTime $heure
     */
    public function setHeure($heure)
    {
        $this->heure = $heure;
    }

}
