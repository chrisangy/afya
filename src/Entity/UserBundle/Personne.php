<?php

namespace App\Entity\UserBundle;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

///**
//* Personne
//*
//* @ORM\Table(name="personne")
//* @ORM\Entity(repositoryClass="UserBundle\Repository\PersonneRepository")
///

/**
 * @ORM\Table(name="personne")
 * @ORM\Entity(repositoryClass=App\Repository\UserBundle\PersonneRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"infirmiere" = App\Entity\UserBundle\Infirmiere",
 *     "medecin" = App\Entity\UserBundle\Medecin",
 *     "assistante" = App\Entity\UserBundle\Assistante",
 *     "patient" = App\Entity\UserBundle\Patient",
 *     "laborantin" = App\Entity\UserBundle\Laborantin",
 *      "pere" =App\Entity\UserBundle\Pere",
 *      "enfants" =App\Entity\UserBundle\Enfants",
 *      "sage_femme" =App\Entity\UserBundle\SageFemme",
 *      "autre_employe" =App\Entity\UserBundle\AutreEmploye",
 *     "patient_standard" = App\Entity\UserBundle\PatientStandard",
 *     "patient_maternite" = App\Entity\UserBundle\PatientMaternite",
 *     "client_non_patient" = App\Entity\UserBundle\ClientNonPatient",
 * })
 * @Vich\Uploadable
 */
abstract class Personne
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libUnique", type="string", length=255)
     */
    private $libUnique = "";
    /**
     * @var string
     *
     * @ORM\Column(name="matricule", type="string", length=255,nullable=true,unique=true)
     */
    private $matricule = "";

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    private $prenom;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_naissance", type="datetime",nullable=true)
     */
    private $dateNaissance;

    /**
     * @var integer
     *
     * @ORM\Column(name="age", type="integer", length=13, nullable=true)
     */
    private $age;

    /**
     * @var string
     *
     * @ORM\Column(name="ifu", type="string", length=13, nullable=true)
     */
    private $ifu;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=13, nullable=true)
     */
    private $telephone;
    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", length=13, nullable=true)
     */
    private $sexe = "";
    /**
     * @var string
     *
     * @ORM\Column(name="ethnie", type="string", length=13, nullable=true)
     */
    private $ethnie = "";
    /**
     * @var string
     *
     * @ORM\Column(name="religion", type="string", length=13, nullable=true)
     */
    private $religion = "";


    /**
     * @var string
     *
     * @ORM\Column(name="quartier", type="string", length=255, nullable=true)
     */
    private $quartier = "";
    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     */
    private $adresse = "";
    /**
     * @var string
     *
     * @ORM\Column(name="lot", type="string", length=255, nullable=true)
     */
    private $lot = "";

    /**
     * @var string
     *
     * @ORM\Column(name="maison", type="string", length=255, nullable=true)
     */
    private $maison = "";

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="string", length=255, nullable=true)
     */
    private $fullname = "";


    /**
     * @var string
     *
     * @ORM\Column(name="personneAContacter", type="string", length=255, nullable=true)
     */
    private $personneAContacter = "";
    /**
     * @var string
     *
     * @ORM\Column(name="telephonePersonneAContacter", type="string", length=255, nullable=true)
     */
    private $telephonePersonneAContacter = "";

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     *
     * @var string
     */
    private $photo;


    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="personne_image", fileNameProperty="photo")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetimetz")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;


    public function __construct()
    {
        $this->deleted = false;
        $this->created = new \Datetime();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libUnique
     *
     * @param string $libUnique
     *
     * @return Personne
     */
    public function setLibUnique($libUnique)
    {
        $this->libUnique = $libUnique;

        return $this;
    }

    /**
     * Get libUnique
     *
     * @return string
     */
    public function getLibUnique()
    {
        return $this->libUnique;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Personne
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Personne
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Personne
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     *
     * @return Personne
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set supprimer
     *
     * @param boolean $supprimer
     *
     * @return Personne
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get supprimer
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Personne
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }


    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Personne
     */
    public function setDateNaissance($dateNaissance)
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function geDateNaissance()
    {
        return $this->dateNaissance;
    }

    /**
     * Get dateNaissance
     *
     * @return \DateTime
     */
    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    public function __toString()
    {
        return (string)$this->fullname;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     *
     * @return Personne
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set ethnie
     *
     * @param string $ethnie
     *
     * @return Personne
     */
    public function setEthnie($ethnie)
    {
        $this->ethnie = $ethnie;

        return $this;
    }

    /**
     * Get ethnie
     *
     * @return string
     */
    public function getEthnie()
    {
        return $this->ethnie;
    }

    /**
     * Set religion
     *
     * @param string $religion
     *
     * @return Personne
     */
    public function setReligion($religion)
    {
        $this->religion = $religion;

        return $this;
    }

    /**
     * Get religion
     *
     * @return string
     */
    public function getReligion()
    {
        return $this->religion;
    }

    /**
     * Set quartier
     *
     * @param string $quartier
     *
     * @return Personne
     */
    public function setQuartier($quartier)
    {
        $this->quartier = $quartier;

        return $this;
    }

    /**
     * Get quartier
     *
     * @return string
     */
    public function getQuartier()
    {
        return $this->quartier;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Personne
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set lot
     *
     * @param string $lot
     *
     * @return Personne
     */
    public function setLot($lot)
    {
        $this->lot = $lot;

        return $this;
    }

    /**
     * Get lot
     *
     * @return string
     */
    public function getLot()
    {
        return $this->lot;
    }

    /**
     * Set personneAContacter
     *
     * @param string $personneAContacter
     *
     * @return Personne
     */
    public function setPersonneAContacter($personneAContacter)
    {
        $this->personneAContacter = $personneAContacter;

        return $this;
    }

    /**
     * Get personneAContacter
     *
     * @return string
     */
    public function getPersonneAContacter()
    {
        return $this->personneAContacter;
    }

    /**
     * Set maison
     *
     * @param string $maison
     *
     * @return Personne
     */
    public function setMaison($maison)
    {
        $this->maison = $maison;

        return $this;
    }

    /**
     * Get maison
     *
     * @return string
     */
    public function getMaison()
    {
        return $this->maison;
    }

    /**
     * Set telephonePersonneAContacter
     *
     * @param string $telephonePersonneAContacter
     *
     * @return Personne
     */
    public function setTelephonePersonneAContacter($telephonePersonneAContacter)
    {
        $this->telephonePersonneAContacter = $telephonePersonneAContacter;

        return $this;
    }

    /**
     * Get telephonePersonneAContacter
     *
     * @return string
     */
    public function getTelephonePersonneAContacter()
    {
        return $this->telephonePersonneAContacter;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return Personne
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }


    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     * @throws \Exception
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Personne
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set matricule
     *
     * @param string $matricule
     *
     * @return Personne
     */
    public function setMatricule($matricule)
    {
        $this->matricule = $matricule;

        return $this;
    }

    /**
     * Get matricule
     *
     * @return string
     */
    public function getMatricule()
    {
        return $this->matricule;
    }


    /**
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param int $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }


    /**
     * Set ifu
     *
     * @param string $ifu
     *
     * @return Personne
     */
    public function setIfu($ifu)
    {
        $this->ifu = $ifu;

        return $this;
    }

    /**
     * Get ifu
     *
     * @return string
     */
    public function getIfu()
    {
        return $this->ifu;
    }
}
