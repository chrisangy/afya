<?php

namespace App\Entity\UserBundle;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\UserBundle\Personne;


/**
 * Assistante
 *
 * @ORM\Table(name="assistante")
 * @ORM\Entity(repositoryClass=App\Repository\UserBundle\AssistanteRepository")
 */
class Assistante extends Personne
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
