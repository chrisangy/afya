<?php

namespace App\Entity\UserBundle;

use Doctrine\ORM\Mapping as ORM;

/**
 * Medecin
 *
 * @ORM\Table(name="medecin")
 * @ORM\Entity(repositoryClass=App\Repository\UserBundle\MedecinRepository")
 */


class Medecin  extends Personne
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\Specialite")
     * @ORM\JoinColumn(nullable=false)
     */
    private $specialite;


    public function __construct()
    {
        parent::__construct();

    }


}
