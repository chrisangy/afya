<?php

namespace App\Entity\UserBundle;

use Doctrine\ORM\Mapping as ORM;

/**
 * PatientMaternite
 *
 * @ORM\Table(name="patient_maternite")
 * @ORM\Entity(repositoryClass=App\Repository\UserBundle\PatientMaterniteRepository")
 */
class PatientMaternite extends Patient
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="vacAntitetanique", type="datetime",nullable=true)
     */
    private $vacAntitetanique;

    /**
     * @var bool
     *
     * @ORM\Column(name="VAT2", type="datetime",nullable=true)
     */
    private $vAT2;

    /**
     * @var bool
     *
     * @ORM\Column(name="VAT3", type="datetime",nullable=true)
     */
    private $vAT3;

    /**
     * @var bool
     *
     * @ORM\Column(name="VAT4", type="datetime",nullable=true)
     */
    private $vAT4;

    /**
     * @var string
     *
     * @ORM\Column(name="VAT5", type="datetime", length=255,nullable=true)
     */
    private $vAT5;

    /**
     * @var bool
     *
     * @ORM\Column(name="estCompletementVac", type="boolean")
     */
    private $estCompletementVac = false;

    public function __construct()
    {
        parent::__construct();
        $this->setSexe("F");
        

    }


    
    /**
     * Set vacAntitetanique
     *
     * @param \DateTime $vacAntitetanique
     *
     * @return PatientMaternite
     */
    public function setVacAntitetanique($vacAntitetanique)
    {
        $this->vacAntitetanique = $vacAntitetanique;

        return $this;
    }

    /**
     * Get vacAntitetanique
     *
     * @return \DateTime
     */
    public function getVacAntitetanique()
    {
        return $this->vacAntitetanique;
    }

    /**
     * Set vAT2
     *
     * @param \DateTime $vAT2
     *
     * @return PatientMaternite
     */
    public function setVAT2($vAT2)
    {
        $this->vAT2 = $vAT2;

        return $this;
    }

    /**
     * Get vAT2
     *
     * @return \DateTime
     */
    public function getVAT2()
    {
        return $this->vAT2;
    }

    /**
     * Set vAT3
     *
     * @param \DateTime $vAT3
     *
     * @return PatientMaternite
     */
    public function setVAT3($vAT3)
    {
        $this->vAT3 = $vAT3;

        return $this;
    }

    /**
     * Get vAT3
     *
     * @return \DateTime
     */
    public function getVAT3()
    {
        return $this->vAT3;
    }

    /**
     * Set vAT4
     *
     * @param \DateTime $vAT4
     *
     * @return PatientMaternite
     */
    public function setVAT4($vAT4)
    {
        $this->vAT4 = $vAT4;

        return $this;
    }

    /**
     * Get vAT4
     *
     * @return \DateTime
     */
    public function getVAT4()
    {
        return $this->vAT4;
    }

    /**
     * Set vAT5
     *
     * @param \DateTime $vAT5
     *
     * @return PatientMaternite
     */
    public function setVAT5($vAT5)
    {
        $this->vAT5 = $vAT5;

        return $this;
    }

    /**
     * Get vAT5
     *
     * @return \DateTime
     */
    public function getVAT5()
    {
        return $this->vAT5;
    }

    /**
     * Set estCompletementVac
     *
     * @param boolean $estCompletementVac
     *
     * @return PatientMaternite
     */
    public function setEstCompletementVac($estCompletementVac)
    {
        $this->estCompletementVac = $estCompletementVac;

        return $this;
    }

    /**
     * Get estCompletementVac
     *
     * @return boolean
     */
    public function getEstCompletementVac()
    {
        return $this->estCompletementVac;
    }

}
