<?php

namespace App\Entity\UserBundle;

use AppBundle\TraitClass\EntityDateTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * ClientNonPatient
 *
 * @ORM\Table(name="client_non_patient")
 * @ORM\Entity(repositoryClass=App\Repository\UserBundle\ClientNonPatientRepository")
 */
class ClientNonPatient  extends Personne
{

//    use EntityDateTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="estPatient", type="boolean")
     */
    private $estPatient = false;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


//    /**
//     * Set matricule
//     *
//     * @param string $matricule
//     *
//     * @return ClientNonPatient
//     */
//    public function setMatricule($matricule)
//    {
//        $this->matricule = $matricule;
//
//        return $this;
//    }
//
//    /**
//     * Get matricule
//     *
//     * @return string
//     */
//    public function getMatricule()
//    {
//        return $this->matricule;


    /**
     * Set estPatient
     *
     * @param boolean $estPatient
     *
     * @return ClientNonPatient
     */
    public function setEstPatient($estPatient)
    {
        $this->estPatient = $estPatient;

        return $this;
    }

    /**
     * Get estPatient
     *
     * @return bool
     */
    public function getEstPatient()
    {
        return $this->estPatient;
    }
}
