<?php

namespace App\Entity\UserBundle;

use Doctrine\ORM\Mapping as ORM;

/**
 * AutreEmploye
 *
 * @ORM\Table(name="autre_employe")
 * @ORM\Entity(repositoryClass=App\Repository\UserBundle\AutreEmployeRepository")
 */
class AutreEmploye extends Personne
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
