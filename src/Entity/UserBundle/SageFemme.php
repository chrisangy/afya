<?php

namespace App\Entity\UserBundle;

use Doctrine\ORM\Mapping as ORM;

/**
 * SageFemme
 *
 * @ORM\Table(name="sage_femme")
 * @ORM\Entity(repositoryClass=App\Repository\UserBundle\SageFemmeRepository")
 */
class SageFemme extends Personne
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
