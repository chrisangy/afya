<?php

namespace App\Entity\UserBundle;

use Doctrine\ORM\Mapping as ORM;

/**
 * PatientStandard
 *
 * @ORM\Table(name="patient_standard")
 * @ORM\Entity(repositoryClass=App\Repository\UserBundle\PatientStandardRepository")
 */
class PatientStandard extends Patient
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
