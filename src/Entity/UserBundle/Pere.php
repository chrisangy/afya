<?php

namespace App\Entity\UserBundle;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pere
 *
 * @ORM\Table(name="pere")
 * @ORM\Entity(repositoryClass=App\Repository\UserBundle\PereRepository")
 */
class Pere extends Personne
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="profession", type="string",nullable=true)
     */
    private $profession;
    /**
     * @var bool
     *
     * @ORM\Column(name="emmel", type="boolean")
     */
    private $emmel;

    /**
     * @var bool
     *
     * @ORM\Column(name="GSRH", type="boolean")
     */
    private $gsrh;

    /**
     * @var bool
     *
     * @ORM\Column(name="syphilis", type="boolean")
     */
    private $syphilis;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set emmel
     *
     * @param boolean $emmel
     *
     * @return Pere
     */
    public function setEmmel($emmel)
    {
        $this->emmel = $emmel;

        return $this;
    }

    /**
     * Get emmel
     *
     * @return bool
     */
    public function getEmmel()
    {
        return $this->emmel;
    }

    /**
     * Set gsrh
     *
     * @param boolean $gsrh
     *
     * @return Pere
     */
    public function setGsrh($gsrh)
    {
        $this->gsrh = $gsrh;

        return $this;
    }

    /**
     * Get gsrh
     *
     * @return boolean
     */
    public function getGsrh()
    {
        return $this->gsrh;
    }

    /**
     * Set syphilis
     *
     * @param boolean $syphilis
     *
     * @return Pere
     */
    public function setSyphilis($syphilis)
    {
        $this->syphilis = $syphilis;

        return $this;
    }

    /**
     * Get syphilis
     *
     * @return boolean
     */
    public function getSyphilis()
    {
        return $this->syphilis;
    }

    /**
     * Set profession
     *
     * @param string $profession
     *
     * @return Pere
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;

        return $this;
    }

    /**
     * Get profession
     *
     * @return string
     */
    public function getProfession()
    {
        return $this->profession;
    }
}
