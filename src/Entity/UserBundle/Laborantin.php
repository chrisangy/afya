<?php

namespace App\Entity\UserBundle;

use Doctrine\ORM\Mapping as ORM;
/**
 * Laborantin
 *
 * @ORM\Table(name="laborantin")
 * @ORM\Entity(repositoryClass=App\Repository\UserBundle\LaborantinRepository")
 */
class Laborantin  extends Personne
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;



    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AppBundle\Laboratoire")
     * @ORM\JoinColumn(nullable=false)
     */
    private $laboratoire;

}
