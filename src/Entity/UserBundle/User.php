<?php

namespace App\Entity\UserBundle;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use UserBundle\TraitClass\TypePersonneTrait;

/**
 * @ORM\MappedSuperclass
  * @ORM\Entity
  * @ORM\Table(name="user")
  */
class User extends BaseUser
{
    use TypePersonneTrait;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\OneToOne(targetEntity="UserBundle\Entity\Personne",cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $personne;

    protected $roles;

    public function __construct()
    {
        parent::__construct();
        $this->enabled =  true;
    }


    /**
     * Set personne
     *
     * @param \UserBundle\Entity\Personne $personne
     *
     * @return User
     */
    public function setPersonne(\UserBundle\Entity\Personne $personne = null)
    {
        $this->personne = $personne;

        return $this;
    }

    /**
     * Get personne
     *
     * @return \UserBundle\Entity\Personne
     */
    public function getPersonne()
    {
        return $this->personne;
    }
}
