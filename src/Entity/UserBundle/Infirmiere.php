<?php

namespace App\Entity\UserBundle;

use Doctrine\ORM\Mapping as ORM;


/**
 * Infirmiere
 *
 * @ORM\Table(name="infirmiere")
 * @ORM\Entity(repositoryClass=App\Repository\UserBundle\InfirmiereRepository")
 */
class Infirmiere extends Personne
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="niveauEtude", type="string", length=255, nullable=true)
     */
    private $niveauEtude;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set niveauEtude
     *
     * @param string $niveauEtude
     *
     * @return Infirmiere
     */
    public function setNiveauEtude($niveauEtude)
    {
        $this->niveauEtude = $niveauEtude;

        return $this;
    }

    /**
     * Get niveauEtude
     *
     * @return string
     */
    public function getNiveauEtude()
    {
        return $this->niveauEtude;
    }
}
