<?php

namespace App\Entity\UserBundle;
use AppBundle\TraitClass\EntityDateTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * PatientMaternite
 *
 * @ORM\Table(name="pregancy")
 * @ORM\Entity(repositoryClass=App\Repository\UserBundle\PregnancyRepository")
 */
class Pregnancy
{
    use EntityDateTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="GSRH", type="string" ,length=20,nullable=true)
     */
    private $gSRH = "";

    /**
     * @var bool
     *
     * @ORM\Column(name="syphylis", type="boolean",nullable=true)
     */
    private $syphylis =false;

    /**
     * @var bool
     *
     * @ORM\Column(name="emmel", type="string",length=30,nullable=true)
     */
    private $emmel = "";

    /**
     * @var bool
     *
     * @ORM\Column(name="HIV", type="string")
     */
    private $hIV = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="ATCD", type="boolean")
     */
    private $aTCD = false;

    /**
     * @var float
     *
     * @ORM\Column(name="taille", type="float")
     */
    private $taille;

    /**
     * @var bool
     *
     * @ORM\Column(name="gestite", type="boolean")
     */
    private $gestite = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="parite", type="boolean")
     */
    private $parite = false;

    /**
     * @var int
     *
     * @ORM\Column(name="nbreCesarienne", type="integer")
     */
    private $nbreCesarienne = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="nbreEnfantVivant", type="integer")
     */
    private $nbreEnfantVivant = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="AgHbs", type="string",length=20,nullable=true)
     */
    private $agHbs;

    /**
     * @var int
     *
     * @ORM\Column(name="nbreAvortementSpontane", type="integer")
     */
    private $nbreAvortementSpontane = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="nbreAvortementProvoque", type="integer")
     */
    private $nbreAvortementProvoque = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="nbreCuretage", type="integer")
     */
    private $nbreCuretage = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="grossesseActuelle", type="boolean")
     */
    private $grossesseActuelle =false;

    /**
     * @var bool
     *
     * @ORM\Column(name="DDR", type="datetime",nullable=true)
     */
    private $dDR;

    /**
     * @var string
     *
     * @ORM\Column(name="termeGrossesse", type="datetime", length=255)
     */
    private $termeGrossesse ;

    /**
     * @var int
     *
     * @ORM\Column(name="ageGrossesse", type="string")
     */
    private $ageGrossesse ;


    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\UserBundle\Pere",cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $pere;
    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\UserBundle\Enfants",cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $enfants;

    /**
     * @var string
     *
     * @ORM\Column(name="autreInfo", type="text", length=255,nullable=true)
     */
    private $autreInfo;
  

     /**
     * @ORM\ManyToOne(targetEntity=App\Entity\UserBundle\PatientMaternite")
     * @ORM\JoinColumn(nullable=true)
     */
    private $mother;

    public function __construct()
    {
        $this->createdAt=  new \DateTime();
        $this->deleted= false;
        $this->DDR =new \DateTime();
        
        $this->agHbs= "";
        $this->hIV=false;
        $this->termeGrossesse=new \DateTime();
        $this->taille='0';

    }



}
