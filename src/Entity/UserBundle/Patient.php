<?php

namespace App\Entity\UserBundle;

use Doctrine\ORM\Mapping as ORM;

/**
 * Patient
 *
 * @ORM\Table(name="patient")
 * @ORM\Entity(repositoryClass=App\Repository\UserBundle\PatientRepository")
 *
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "patient_standard" = App\Entity\UserBundle\PatientStandard",
 *     "patient_maternite" = App\Entity\UserBundle\PatientMaternite",
 *     })
 *
 */
abstract class Patient extends Personne
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    protected $id;
   /**
     * @var string
     *
     * @ORM\Column(name="typePatient", type="string",length=30,nullable=true)
     */
    private $typePatient = "";

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typePatient
     *
     * @param string $typePatient
     *
     * @return Patient
     */
    public function setTypePatient($typePatient)
    {
        $this->typePatient = $typePatient;

        return $this;
    }

    /**
     * Get typePatient
     *
     * @return string
     */
    public function getTypePatient()
    {
        return $this->typePatient;
    }
}
