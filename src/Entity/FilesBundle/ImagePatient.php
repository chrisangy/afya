<?php

namespace App\Entity\FilesBundle;

use Doctrine\ORM\Mapping as ORM;

class ImagePatient extends Image
{
    protected $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    private $patient;


    /**
     * Set patient
     *
     * @param \App\Entity\UserBundle\PatientStandard $patient
     *
     * @return ImagePatient
     */
    public function setPatient(\App\Entity\UserBundle\PatientStandard $patient)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return \App\Entity\UserBundle\PatientStandard
     */
    public function getPatient()
    {
        return $this->patient;
    }
}
