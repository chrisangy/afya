<?php

namespace App\Entity\FilesBundle;

use AppBundle\TraitClass\EntityDateTrait;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\InheritanceType;
use Symfony\Component\Validator\Constraints as Assert;



abstract class Image
{
    use EntityDateTrait;

    protected $id;


    private $nomFichier;

    /**
     * @return mixed
     */
    public function getNomFichier()
    {
        return $this->nomFichier;
    }

    /**
     * @param mixed $nomFichier
     * @return Image
     */
    public function setNomFichier($nomFichier)
    {
        $this->nomFichier = $nomFichier;
        return $this;
    }

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $alt;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getAlt(): ?string
    {
        return $this->alt;
    }

    public function setAlt(string $alt): self
    {
        $this->alt = $alt;

        return $this;
    }
}
