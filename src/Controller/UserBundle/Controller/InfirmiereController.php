<?php

namespace App\Controller\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use UserBundle\Entity\Infirmiere;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Infirmiere controller.
 *
 * @Route("infirmiere")
 */
class InfirmiereController extends AbstractController
{
    /**
     * Lists all infirmiere entities.
     *
     * @Route("/", name="infirmiere_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $infirmieres = $em->getRepository('UserBundle:Infirmiere')->findBy([],['id'=>'DESC']);

        return $this->render('infirmiere/index.html.twig', array(
            'infirmieres' => $infirmieres,
        ));
    }

    /**
     * Creates a new infirmiere entity.
     *
     * @Route("/new", name="infirmiere_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $infirmiere = new Infirmiere();
        $form = $this->createForm('UserBundle\Form\InfirmiereType', $infirmiere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $infirmiere->setLibUnique('infirmiere');

//            $user  = $infirmiere->getUser();
//            $user->addRole('ROLE_INFIRMIERE');
            if ($infirmiere->getAge() === null && $infirmiere->getDateNaissance() !== null) {
                $today = new \DateTime('now');
                $dateOfBirth = $infirmiere->getDateNaissance();
                $age = date_diff(date_create($dateOfBirth->format('Y-m-d')), date_create($today->format('Y-m-d')));

                $infirmiere->setAge((int)$age->format('%y'));
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($infirmiere);
            $em->flush();
            $this->addFlash("success", "Enregistrement éffectué avec succès");
            return $this->redirectToRoute('infirmiere_index');
        }

        return $this->render('infirmiere/new.html.twig', array(
            'infirmiere' => $infirmiere,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a infirmiere entity.
     *
     * @Route("/{id}", name="infirmiere_show")
     * @Method("GET")
     */
    public function showAction(Infirmiere $infirmiere)
    {
        $deleteForm = $this->createDeleteForm($infirmiere);

        return $this->render('infirmiere/show.html.twig', array(
            'infirmiere' => $infirmiere,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing infirmiere entity.
     *
     * @Route("/{id}/edit", name="infirmiere_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Infirmiere $infirmiere)
    {
        $deleteForm = $this->createDeleteForm($infirmiere);
        $editForm = $this->createForm('UserBundle\Form\InfirmiereType', $infirmiere);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($infirmiere->getAge() === null && $infirmiere->getDateNaissance() !== null) {
                $today = new \DateTime('now');
                $dateOfBirth = $infirmiere->getDateNaissance();
                $age = date_diff(date_create($dateOfBirth->format('Y-m-d')), date_create($today->format('Y-m-d')));

                $infirmiere->setAge((int)$age->format('%y'));
            }
            $em->flush();
            $this->addFlash("success", "Modification éffectuée avec succès");
            return $this->redirectToRoute('infirmiere_edit', array('id' => $infirmiere->getId()));
        }

        return $this->render('infirmiere/edit.html.twig', array(
            'infirmiere' => $infirmiere,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a infirmiere entity.
     *
     * @Route("/{id}", name="infirmiere_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Infirmiere $infirmiere)
    {
        $form = $this->createDeleteForm($infirmiere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($infirmiere);
            $em->flush();
        }

        return $this->redirectToRoute('infirmiere_index');
    }

    /**
     * Creates a form to delete a infirmiere entity.
     *
     * @param Infirmiere $infirmiere The infirmiere entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Infirmiere $infirmiere)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('infirmiere_delete', array('id' => $infirmiere->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
