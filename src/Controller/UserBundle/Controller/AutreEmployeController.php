<?php

namespace App\Controller\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use UserBundle\Entity\AutreEmploye;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Autreemploye controller.
 *
 * @Route("autreemploye")
 */
class AutreEmployeController extends AbstractController
{
    /**
     * Lists all autreEmploye entities.
     *
     * @Route("/", name="autreemploye_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $autreEmployes = $em->getRepository('UserBundle:AutreEmploye')->findAll();

        return $this->render('autreemploye/index.html.twig', array(
            'autreEmployes' => $autreEmployes,
        ));
    }

    /**
     * Creates a new autreEmploye entity.
     *
     * @Route("/new", name="autreemploye_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $autreEmploye = new Autreemploye();
        $form = $this->createForm('UserBundle\Form\AutreEmployeType', $autreEmploye);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //dump($autreEmploye);die;

            $autreEmploye->setLibUnique('autreemploye');
            $autreEmploye->setLibUnique('autre_employe');
            $em = $this->getDoctrine()->getManager();
            if ($autreEmploye->getAge() === null && $autreEmploye->getDateNaissance() !== null) {
                $today = new \DateTime('now');
                $dateOfBirth = $autreEmploye->getDateNaissance();
                $age = date_diff(date_create($dateOfBirth->format('Y-m-d')), date_create($today->format('Y-m-d')));

                $autreEmploye->setAge((int)$age->format('%y'));
            }
            $em->persist($autreEmploye);
            $em->flush();
            $this->addFlash("success", "Enregistrement éffectué avec succès");
            return $this->redirectToRoute('autreemploye_show', array('id' => $autreEmploye->getId()));
        }

        return $this->render('autreemploye/new.html.twig', array(
            'autreEmploye' => $autreEmploye,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a autreEmploye entity.
     *
     * @Route("/{id}", name="autreemploye_show")
     * @Method("GET")
     */
    public function showAction(AutreEmploye $autreEmploye)
    {
        $deleteForm = $this->createDeleteForm($autreEmploye);

        return $this->render('autreemploye/show.html.twig', array(
            'autreEmploye' => $autreEmploye,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing autreEmploye entity.
     *
     * @Route("/{id}/edit", name="autreemploye_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, AutreEmploye $autreEmploye)
    {
        $deleteForm = $this->createDeleteForm($autreEmploye);
        $editForm = $this->createForm('UserBundle\Form\AutreEmployeType', $autreEmploye);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($autreEmploye->getAge() === null && $autreEmploye->getDateNaissance() !== null) {
                $today = new \DateTime('now');
                $dateOfBirth = $autreEmploye->getDateNaissance();
                $age = date_diff(date_create($dateOfBirth->format('Y-m-d')), date_create($today->format('Y-m-d')));

                $autreEmploye->setAge((int)$age->format('%y'));
            }
            $em->flush();
            $this->addFlash("success", "Modification éffectuée avec succès");
            return $this->redirectToRoute('autreemploye_edit', array('id' => $autreEmploye->getId()));
        }

        return $this->render('autreemploye/edit.html.twig', array(
            'autreEmploye' => $autreEmploye,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a autreEmploye entity.
     *
     * @Route("/{id}", name="autreemploye_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, AutreEmploye $autreEmploye)
    {
        $form = $this->createDeleteForm($autreEmploye);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($autreEmploye);
            $em->flush();
        }

        return $this->redirectToRoute('autreemploye_index');
    }

    /**
     * Creates a form to delete a autreEmploye entity.
     *
     * @param AutreEmploye $autreEmploye The autreEmploye entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(AutreEmploye $autreEmploye)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('autreemploye_delete', array('id' => $autreEmploye->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
