<?php

namespace App\Controller\UserBundle\Controller;

use AppBundle\Entity\Operation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use UserBundle\Entity\ClientNonPatient;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Clientnonpatient controller.
 *
 * @Route("clientnonpatient")
 */
class ClientNonPatientController extends AbstractController
{
    /**
     * Lists all clientNonPatient entities.
     *
     * @Route("/", name="clientnonpatient_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $clientNonPatients = $em->getRepository('UserBundle:ClientNonPatient')->findAll();

        return $this->render('clientnonpatient/index.html.twig', array(
            'clientNonPatients' => $clientNonPatients,
        ));
    }

    /**
     * Creates a new clientNonPatient entity.
     *
     * @Route("/new", name="clientnonpatient_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $clientNonPatient = new Clientnonpatient();
        $form = $this->createForm('UserBundle\Form\ClientNonPatientType', $clientNonPatient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
           // $uniqueId = md5(uniqid());
//            $clientNonPatient->setMatricule($uniqueId);
            $clientNonPatient->setLibUnique('client_non_patient');
//            dump($clientNonPatient);
//            die;
//            $clientNonPatient->setDeleted(false);
//            $clientNonPatient->setCreated(new \DateTime());


            $em->persist($clientNonPatient);
            $em->flush();
            $this->addFlash("success", "Enregistrement éffectué avec succès");
            return $this->redirectToRoute('clientnonpatient_show', array('id' => $clientNonPatient->getId()));
        }

        return $this->render('clientnonpatient/new.html.twig', array(
            'clientNonPatient' => $clientNonPatient,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new clientNonPatient entity.
     *
     * @Route("/modal/new", name="clientnonpatient_modal_new")
     * @Method({"GET", "POST"})
     */
    public function modalNewAction(Request $request)
    {
        $clientNonPatient = new Clientnonpatient();
        $form = $this->createForm('UserBundle\Form\ClientNonPatientType', $clientNonPatient);
        $form->handleRequest($request);

//        dump($request->getMethod() );
//        die;
        if ($request->getMethod() == 'POST') {
            $em = $this->getDoctrine()->getManager();
            $uniqueId = md5(uniqid());
            $clientNonPatient->setMatricule($uniqueId);
            $em->persist($clientNonPatient);
            $em->flush();

            $this->addFlash("success", " le client a été correctement crée et est disponible dans le type Client");
            return $this->redirectToRoute('operation_new');
        }

        return $this->render('clientnonpatient/modalNew.html.twig', array(
            'clientNonPatient' => $clientNonPatient,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a clientNonPatient entity.
     *
     * @Route("/{id}", name="clientnonpatient_show")
     * @Method("GET")
     */
    public function showAction(ClientNonPatient $clientNonPatient)
    {


        $em = $this->getDoctrine()->getManager();
        $deleteForm = $this->createDeleteForm($clientNonPatient);
        $operation= $em->getRepository(Operation::class)->findBy(['patient'=> $clientNonPatient],['id'=>'DESC'],50);

//        dump($clientNonPatient);
//        die;
        return $this->render('clientnonpatient/show.html.twig', array(
            'patientStandard' => $clientNonPatient,
            'operations' => $operation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing clientNonPatient entity.
     *
     * @Route("/{id}/edit", name="clientnonpatient_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ClientNonPatient $clientNonPatient)
    {
        $deleteForm = $this->createDeleteForm($clientNonPatient);
        $editForm = $this->createForm('UserBundle\Form\ClientNonPatientType', $clientNonPatient);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", 'Modification éffectuée avec succès');
            return $this->redirectToRoute('clientnonpatient_edit', array('id' => $clientNonPatient->getId()));
        }

        return $this->render('clientnonpatient/edit.html.twig', array(
            'clientNonPatient' => $clientNonPatient,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a clientNonPatient entity.
     *
     * @Route("/{id}", name="clientnonpatient_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ClientNonPatient $clientNonPatient)
    {
        $form = $this->createDeleteForm($clientNonPatient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($clientNonPatient);
            $em->flush();
        }

        return $this->redirectToRoute('clientnonpatient_index');
    }

    /**
     * Creates a form to delete a clientNonPatient entity.
     *
     * @param ClientNonPatient $clientNonPatient The clientNonPatient entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ClientNonPatient $clientNonPatient)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('clientnonpatient_delete', array('id' => $clientNonPatient->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
