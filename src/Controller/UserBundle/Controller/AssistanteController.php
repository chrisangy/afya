<?php

namespace App\Controller\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use UserBundle\Entity\Assistante;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Assistante controller.
 *
 * @Route("assistante")
 */
class AssistanteController extends AbstractController
{
    /**
     * Lists all assistante entities.
     *
     * @Route("/", name="assistante_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $assistantes = $em->getRepository('UserBundle:Assistante')->findAll();

        return $this->render('assistante/index.html.twig', array(
            'assistantes' => $assistantes,
        ));
    }

    /**
     * Creates a new assistante entity.
     *
     * @Route("/new", name="assistante_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $assistante = new Assistante();
        $form = $this->createForm('UserBundle\Form\AssistanteType', $assistante);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $assistante->setLibUnique('assistante');

            $user  = $assistante->getUser();
            $user->addRole('ROLE_ASSISTANTE');
            $em = $this->getDoctrine()->getManager();
            $em->persist($assistante);
            $em->flush();

            return $this->redirectToRoute('assistante_show', array('id' => $assistante->getId()));
        }

        return $this->render('assistante/new.html.twig', array(
            'assistante' => $assistante,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a assistante entity.
     *
     * @Route("/{id}", name="assistante_show")
     * @Method("GET")
     */
    public function showAction(Assistante $assistante)
    {
        $deleteForm = $this->createDeleteForm($assistante);

        return $this->render('assistante/show.html.twig', array(
            'assistante' => $assistante,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing assistante entity.
     *
     * @Route("/{id}/edit", name="assistante_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Assistante $assistante)
    {
        $deleteForm = $this->createDeleteForm($assistante);
        $editForm = $this->createForm('UserBundle\Form\AssistanteType', $assistante);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('assistante_edit', array('id' => $assistante->getId()));
        }

        return $this->render('assistante/edit.html.twig', array(
            'assistante' => $assistante,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a assistante entity.
     *
     * @Route("/{id}", name="assistante_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Assistante $assistante)
    {
        $form = $this->createDeleteForm($assistante);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($assistante);
            $em->flush();
        }

        return $this->redirectToRoute('assistante_index');
    }

    /**
     * Creates a form to delete a assistante entity.
     *
     * @param Assistante $assistante The assistante entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Assistante $assistante)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('assistante_delete', array('id' => $assistante->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
