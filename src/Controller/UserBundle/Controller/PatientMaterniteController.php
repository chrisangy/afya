<?php

namespace App\Controller\UserBundle\Controller;

use AppBundle\Entity\Consultation;
use AppBundle\Entity\Operation;
use AppBundle\Entity\RegistreSoins;
use AppBundle\Entity\RendezVous;
use AppBundle\StaticClass\ConstanteClass;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\ClientNonPatient;
use UserBundle\Entity\PatientMaternite;
use UserBundle\Entity\Pregnancy;
use UserBundle\Repository\PregnancyRepository;

/**
 * Patientmaternite controller.
 *
 * @Route("patientmaternite")
 */
class PatientMaterniteController extends AbstractController
{
    /**
     * Lists all patientMaternite entities.
     *
     * @Route("/", name="patientmaternite_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $patientMaternites = $em->getRepository('UserBundle:PatientMaternite')->findBy([], ['id' => 'DESC']);

        return $this->render('patientmaternite/index.html.twig', array(
            'patientMaternites' => $patientMaternites,
        ));
    }

    /**
     * Creates a new patientMaternite entity.
     *
     * @Route("/new", name="patientmaternite_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {

        $patientMaternite = new PatientMaternite();
        $pregnancy = new Pregnancy();
        $form = $this->createForm('UserBundle\Form\PatientMaterniteType', $patientMaternite);
        $formpregnancy = $this->createForm('UserBundle\Form\PregnancyType', $pregnancy);
        $form->handleRequest($request);
        $formpregnancy->handleRequest($request);

        if ($form->isSubmitted() /*&& $form->isValid()*/) {
            $em = $this->getDoctrine()->getManager();

            if ($patientMaternite->getAge() === null && $patientMaternite->getDateNaissance() !== null) {
                $today = new \DateTime('now');
                $dateOfBirth = $patientMaternite->getDateNaissance();
                $age = date_diff(date_create($dateOfBirth->format('Y-m-d')), date_create($today->format('Y-m-d')));

                $patientMaternite->setAge((int)$age->format('%y'));
            }
            $patientMaternite->setTypePatient(ConstanteClass::RefTypePatientMaternite);

            $em->persist($patientMaternite);
            $patientMaternite->setLibUnique("patient_maternite");
            
            $pregnancy->setMother($patientMaternite);
            $pregnancy->getPere()->setLibUnique("pere");
            $pregnancy->getEnfants()->setLibUnique("enfants");
            $em->persist($pregnancy);

//            creation du dossier patient
            $this->get('dossier_service')->createDossier($patientMaternite, $request->get('dossier-observation'));

            $em->flush();

            $this->addFlash("success", 'Creation éffectuée avec succès');
            return $this->redirectToRoute('patientmaternite_index');
        }

        return $this->render('patientmaternite/new.html.twig', array(
            'patientMaternite' => $patientMaternite,
            'form' => $form->createView(),
            'formpregnancy' => $formpregnancy->createView(),
        ));
    }

    /**
     * Creates a new patientMaternite entity.
     *
     * @Route("/client/patient/maternite/{matricule}", name="client_to_patient_maternite")
     * @Method({"GET", "POST"})
     */
    public function clientToPatientMaternite(Request $request, ClientNonPatient $clientNonPatient)
    {

        $patientMaternite = new PatientMaternite();
        $patientMaternite->setNom($clientNonPatient->getNom());
        $patientMaternite->setPrenom($clientNonPatient->getPrenom());
        $patientMaternite->setTelephone($clientNonPatient->getTelephone());

        $form = $this->createForm('UserBundle\Form\PatientMaterniteType', $patientMaternite);
        $form->handleRequest($request);

        if ($form->isSubmitted() /*&& $form->isValid()*/) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($patientMaternite);

            $patientMaternite->setLibUnique("patient_maternite");
            $patientMaternite->getPere()->setLibUnique("pere");
            $patientMaternite->getEnfants()->setLibUnique("enfants");


            $clientNonPatient->setEstPatient(true);
            $em->flush();
            $this->addFlash("success", 'Creation éffectuée avec succès');
            return $this->redirectToRoute('patientmaternite_index');
        }

        return $this->render('patientmaternite/new.html.twig', array(
            'patientMaternite' => $patientMaternite,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a patientMaternite entity.
     *
     * @Route("/{matricule}", name="patientmaternite_show")
     * @Method("GET")
     */
    public function showAction(PatientMaternite $patientMaternite)
    {
        $em = $this->getDoctrine()->getManager();
        $operation = $em->getRepository(Operation::class)->findBy(['patient' => $patientMaternite], ['id' => 'DESC'], 50);
        $rdv = $em->getRepository(RendezVous::class)->findFutureRdvPatient($patientMaternite);
        $soins = $em->getRepository(RegistreSoins::class)->findBy(['patient' => $patientMaternite]);
        $grossese = $em->getRepository(Pregnancy::class)->findBy(['mother' => $patientMaternite,'deleted'=>0],['id'=>'DESC']);
        $consultation = $em->getRepository(Consultation::class)->findByPatientObj($patientMaternite);
        return $this->render('patientmaternite/show.html.twig', array(
            'patientMaternite' => $patientMaternite,
            'operations' => $operation,
            'registreSoins' => $soins,
            'grossese' => $grossese,
            'rendezVouses' => $rdv,
            'consultations' => $consultation,
        ));
    }

    /**
     * Displays a form to edit an existing patientMaternite entity.
     *
     * @Route("/{matricule}/{prenancyid}/edit", name="patientmaternite_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PatientMaternite $patientMaternite)
    {
        $editForm = $this->createForm('UserBundle\Form\PatientMaterniteType', $patientMaternite);
        $editForm->handleRequest($request);
        $em= $this->getDoctrine()->getManager();
        $pregnancy=  $em->getRepository(Pregnancy::class)->find($request->get('prenancyid'));
        $formpregnancy = $this->createForm('UserBundle\Form\PregnancyType', $pregnancy);
        $formpregnancy->handleRequest($request);
        if ($editForm->isSubmitted()) {
            if ($patientMaternite->getAge() === null && $patientMaternite->getDateNaissance() !== null) {
                $today = new \DateTime('now');
                $dateOfBirth = $patientMaternite->getDateNaissance();
                $age = date_diff(date_create($dateOfBirth->format('Y-m-d')), date_create($today->format('Y-m-d')));

                $patientMaternite->setAge((int)$age->format('%y'));
            }
            $em->flush();
            $this->addFlash("success", 'Modification éffectuée avec succès');
            return $this->redirectToRoute('patientmaternite_index');
        }

        return $this->render('patientmaternite/edit.html.twig', array(
            'patientMaternite' => $patientMaternite,
            'form' => $editForm->createView(),
            'formpregnancy' => $formpregnancy->createView(),
        ));
    }

    /**
     * @Route("/{matricule}/autre-grosses", name="patientmaternite_grosses_new")
     * @Method({"GET", "POST"})
     */
    public function patientmaternite_grosses_newAction(Request $request, PatientMaternite $patientMaternite)
    {
        $editForm = $this->createForm('UserBundle\Form\PatientMaterniteType', $patientMaternite);
        $editForm->handleRequest($request);
        $em= $this->getDoctrine()->getManager();
        $pregnancy=  new Pregnancy();
        $formpregnancy = $this->createForm('UserBundle\Form\PregnancyType', $pregnancy);
        $formpregnancy->handleRequest($request);
        if ($editForm->isSubmitted()) {
            if ($patientMaternite->getAge() === null && $patientMaternite->getDateNaissance() !== null) {
                $today = new \DateTime('now');
                $dateOfBirth = $patientMaternite->getDateNaissance();
                $age = date_diff(date_create($dateOfBirth->format('Y-m-d')), date_create($today->format('Y-m-d')));

                $patientMaternite->setAge((int)$age->format('%y'));
            }
            $pregnancy->setMother($patientMaternite);
            $pregnancy->getPere()->setLibUnique("pere");
            $pregnancy->getEnfants()->setLibUnique("enfants");
            $em->persist($pregnancy);
            $em->flush();
            $this->addFlash("success", 'Modification éffectuée avec succès');
            return $this->redirectToRoute('patientmaternite_index');
        }

        return $this->render('patientmaternite/grossenew.html.twig', array(
            'patientMaternite' => $patientMaternite,
            'form' => $editForm->createView(),
            'formpregnancy' => $formpregnancy->createView(),
        ));
    }

    /**
     * Deletes a patientMaternite entity.
     *
     * @Route("/{matricule}", name="patientmaternite_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, PatientMaternite $patientMaternite)
    {
        $form = $this->createDeleteForm($patientMaternite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($patientMaternite);
            $em->flush();
        }

        return $this->redirectToRoute('patientmaternite_index');
    }

    /**
     * Creates a form to delete a patientMaternite entity.
     *
     * @param PatientMaternite $patientMaternite The patientMaternite entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PatientMaternite $patientMaternite)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('patientmaternite_delete', array('id' => $patientMaternite->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
