<?php

namespace App\Controller\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use UserBundle\Entity\Personne;
use UserBundle\Entity\User;

/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends AbstractController
{
    /**
     * Lists all user entities.
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('UserBundle:User')->findBy([],['id'=>'DESC']);

        return $this->render('user/index.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        return $this->redirectToRoute('custom_user_new');
        $user = new User();
        $form = $this->createForm('UserBundle\Form\UserType', $user);
        $form->handleRequest($request);

        $ok = array();
        // not finish
        foreach (get_declared_classes() as $class) {
            $test = new $class;
            if ($test instanceof Personne)
                $ok[] = $class;

        }
        dump(get_declared_classes());
        die;


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash("success", "Enregistrement éffectué avec succès");
            return $this->redirectToRoute('user_show', array('id' => $user->getId()));
        }

        return $this->render('user/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/autorise/personnel", name="custom_user_new")
     * @Method({"GET", "POST"})
     */
    public function customNewAction(Request $request)
    {
        $user = new User();

        $form = $this->createForm('UserBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entityPath = $request->get('entityPath');
            $matriculeUser = $request->get('matUser');


            //$personne = $em->getRepository("UserBundle:Personne")->findOneByMatricule($user->$personne->getMatricule());
            $personne = $user->getPersonne();
            $existUser = $em->getRepository("UserBundle:User")->findOneByPersonne($user->getPersonne());
//dump($existUser);die;
            if (!$user->getPersonne()) {
                $this->addFlash("error", "La personne sélectionner n'existe pas");
                goto END;
            }
            if ($existUser) {
                $this->addFlash("error", "Cet utilisateur possede deja des identifiants   de connexion");
                goto END;
            }

            foreach ($request->get('role_user') as $role) {
                $user->addRole($role);
            }

            $user->setPersonne($personne);
            $em->persist($user);
            $em->flush();
            $message = 'Utilisateur cree avec succes';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('user_index');
//            return $this->redirectToRoute('user_show', array('id' => $user->getId()));
        }


        END:
        return $this->render('user/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a user entity.
     *
     * @Route("/{id}", name="user_show")
     * @Method("GET")
     */
    public function showAction(User $user)
    {
        $deleteForm = $this->createDeleteForm($user);

        return $this->render('user/show.html.twig', array(
            'user' => $user,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('UserBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", "Modification éffectuée avec succès");
            return $this->redirectToRoute('user_edit', array('id' => $user->getId()));
        }

        return $this->render('user/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a user entity.
     *
     * @Route("/{id}", name="user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * @Route("/etat/{stat}/{id}",name="changeStat")
     */
    public function changeStatAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $currentuser = $this->getUser();
        $session = new Session();
//        if (in_array('ROLE_ADMIN', $currentuser->getRoles()) || in_array('ROLE_CLIENT', $currentuser->getRoles())) {
//            $message = "You dont have access to that page";
//            $session->getFlashBag()->add('success', $message);
//            return $this->redirectToRoute('dashboard');
//        }
        $stat = $request->get('stat');
        $id = $request->get('id');
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $id));
        // dump($user->getUsername());die();

        $user->setEnabled($stat);
        $userManager->updateUser($user);
        if ($stat == 1) {
            $message = "Le compte de <b>" . $user->getUsername() . "</b> a été reactivée";
        } else {
            $message = "Le compte de <b>" . $user->getUsername() . "</b> a été désactivé";
        }


        $session->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('default_default');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
