<?php

namespace App\Controller\UserBundle\Controller;

use AppBundle\Entity\Consultation;
use AppBundle\Entity\Dossier;
use AppBundle\Entity\Operation;
use AppBundle\Entity\RegistreSoins;
use AppBundle\Entity\RendezVous;
use AppBundle\StaticClass\ConstanteClass;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\ClientNonPatient;
use UserBundle\Entity\PatientStandard;

/**
 * Patientstandard controller.
 *
 * @Route("patientstandard")
 */
class PatientStandardController extends AbstractController
{
    /**
     * Lists all patientStandard entities.
     *
     * @Route("/", name="patientstandard_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $patientStandards = $em->getRepository('UserBundle:PatientStandard')->findBy([], ['id' => 'DESC']);


        return $this->render('patientstandard/index.html.twig', array(
            'patientStandards' => $patientStandards,
        ));
    }

    /**
     * Creates a new patientStandard entity.
     *
     * @Route("/new", name="patientstandard_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $patientStandard = new Patientstandard();
        $form = $this->createForm('UserBundle\Form\PatientStandardType', $patientStandard);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $patientStandard->setTypePatient('patient maternite');
            $patientStandard->setLibUnique("patient_standard")->setTypePatient(ConstanteClass::RefTypePatientStandard);
            if ($patientStandard->getAge() === null && $patientStandard->getDateNaissance() !== null) {
                $today = new \DateTime('now');
                $dateOfBirth = $patientStandard->getDateNaissance();
                $age = date_diff(date_create($dateOfBirth->format('Y-m-d')), date_create($today->format('Y-m-d')));

                $patientStandard->setAge((int)$age->format('%y'));
            }

//            creation du dossier patient
            $dossier = $this->get('dossier_service')->createDossier($patientStandard, "ras");

            $em->persist($patientStandard);
            $em->flush();


            $this->addFlash("success", 'Creation éffectuée avec succès');
            return $this->redirectToRoute('patientstandard_index');
        }

        return $this->render('patientstandard/new.html.twig', array(
            'patientStandard' => $patientStandard,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new patientStandard entity.
     *
     * @Route("/client/patient/standard/{matricule}", name="client_to_patient_standard")
     * @Method({"GET", "POST"})
     */
    public function clientToPatientStandart(Request $request, ClientNonPatient $clientNonPatient)
    {
        $patientStandard = new Patientstandard();

        $patientStandard->setNom($clientNonPatient->getNom());
        $patientStandard->setPrenom($clientNonPatient->getPrenom());
        $patientStandard->setTelephone($clientNonPatient->getTelephone());

        $form = $this->createForm('UserBundle\Form\PatientStandardType', $patientStandard);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /* if ($patientStandard->getAge() === null && $patientStandard->getDateNaissance() !== null) {
                 if ($patientStandard->getAge() === null && $patientStandard->getDateNaissance() !== null) {
                     $today= new \DateTime('now');
                     $dateOfBirth= $patientStandard->getDateNaissance();
                     $age= date_diff(date_create($dateOfBirth->format('Y-m-d')), date_create($today->format('Y-m-d')));

                     $patientStandard->setAge((int)$age->format('%y'));
                 }
             }
             */
            $patientStandard->setLibUnique("patient_standard");
            $em->persist($patientStandard);
            $em->flush();
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", 'Creation éffectuée avec succès');
            return $this->redirectToRoute('patientstandard_index');
        }

        return $this->render('patientstandard/new.html.twig', array(
            'patientStandard' => $patientStandard,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a patientStandard entity.
     *
     * @Route("/{matricule}", name="patientstandard_show")
     * @Method("GET")
     * @param PatientStandard $patientStandard
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(PatientStandard $patientStandard)
    {
        $em = $this->getDoctrine()->getManager();
        $operation = $em->getRepository(Operation::class)->findBy(['patient' => $patientStandard], ['id' => 'DESC'], 50);
        $rdv = $em->getRepository(RendezVous::class)->findFutureRdvPatient($patientStandard);
        $soins = $em->getRepository(RegistreSoins::class)->findBy(['patient' => $patientStandard],['id'=>'DESC']);

        $consultation = $em->getRepository(Consultation::class)->findByPatientObj($patientStandard);
        return $this->render('patientstandard/show.html.twig', array(
            'patientStandard' => $patientStandard,
            'operations' => $operation,
            'registreSoins' => $soins,
            'rendezVouses' => $rdv,
            'consultations' => $consultation,
        ));
    }

    /**
     * Displays a form to edit an existing patientStandard entity.
     *
     * @Route("/{matricule}/edit", name="patientstandard_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param PatientStandard $patientStandard
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, PatientStandard $patientStandard)
    {
//        $deleteForm = $this->createDeleteForm($patientStandard);
        $editForm = $this->createForm('UserBundle\Form\PatientStandardType', $patientStandard);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if ($patientStandard->getAge() === null && $patientStandard->getDateNaissance() !== null) {
                $today = new \DateTime('now');
                $dateOfBirth = $patientStandard->getDateNaissance();
                $age = date_diff(date_create($dateOfBirth->format('Y-m-d')), date_create($today->format('Y-m-d')));

                $patientStandard->setAge((int)$age->format('%y'));
            }

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", 'modification éffectuée avec succès');

            return $this->redirectToRoute('patientstandard_index');
        }

        return $this->render('patientstandard/edit.html.twig', array(
            'patientStandard' => $patientStandard,
            'edit_form' => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a patientStandard entity.
     *
     * @Route("/{matricule}", name="patientstandard_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, PatientStandard $patientStandard)
    {
        $form = $this->createDeleteForm($patientStandard);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($patientStandard);
            $em->flush();
        }

        return $this->redirectToRoute('patientstandard_index');
    }

    /**
     * Creates a form to delete a patientStandard entity.
     *
     * @param PatientStandard $patientStandard The patientStandard entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PatientStandard $patientStandard)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('patientstandard_delete', array('id' => $patientStandard->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
