<?php

namespace App\Controller\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use UserBundle\Entity\Laborantin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Laborantin controller.
 *
 * @Route("laborantin")
 */
class LaborantinController extends AbstractController
{
    /**
     * Lists all laborantin entities.
     *
     * @Route("/", name="laborantin_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $laborantins = $em->getRepository('UserBundle:Laborantin')->findBy([],['id'=>'DESC']);

        return $this->render('laborantin/index.html.twig', array(
            'laborantins' => $laborantins,
        ));
    }

    /**
     * Creates a new laborantin entity.
     *
     * @Route("/new", name="laborantin_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $laborantin = new Laborantin();
        $form = $this->createForm('UserBundle\Form\LaborantinType', $laborantin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $laborantin->setLibUnique('laborantin');

            $user  = $laborantin->getUser();
            $user->addRole('ROLE_LABORANTIN');

            $em = $this->getDoctrine()->getManager();
            $em->persist($laborantin);
            $em->flush();

            return $this->redirectToRoute('laborantin_index');
        }

        return $this->render('laborantin/new.html.twig', array(
            'laborantin' => $laborantin,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a laborantin entity.
     *
     * @Route("/{id}", name="laborantin_show")
     * @Method("GET")
     */
    public function showAction(Laborantin $laborantin)
    {
        $deleteForm = $this->createDeleteForm($laborantin);

        return $this->render('laborantin/show.html.twig', array(
            'laborantin' => $laborantin,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing laborantin entity.
     *
     * @Route("/{id}/edit", name="laborantin_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Laborantin $laborantin)
    {
        $deleteForm = $this->createDeleteForm($laborantin);
        $editForm = $this->createForm('UserBundle\Form\LaborantinType', $laborantin);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('laborantin_edit', array('id' => $laborantin->getId()));
        }

        return $this->render('laborantin/edit.html.twig', array(
            'laborantin' => $laborantin,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a laborantin entity.
     *
     * @Route("/{id}", name="laborantin_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Laborantin $laborantin)
    {
        $form = $this->createDeleteForm($laborantin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($laborantin);
            $em->flush();
        }

        return $this->redirectToRoute('laborantin_index');
    }

    /**
     * Creates a form to delete a laborantin entity.
     *
     * @param Laborantin $laborantin The laborantin entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Laborantin $laborantin)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('laborantin_delete', array('id' => $laborantin->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
