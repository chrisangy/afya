<?php

namespace App\Controller\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use UserBundle\Entity\SageFemme;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Sagefemme controller.
 *
 * @Route("sagefemme")
 */
class SageFemmeController extends AbstractController
{
    /**
     * Lists all sageFemme entities.
     *
     * @Route("/", name="sagefemme_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sageFemmes = $em->getRepository('UserBundle:SageFemme')->findBy([],['id'=>'DESC']);

        return $this->render('sagefemme/index.html.twig', array(
            'sageFemmes' => $sageFemmes,
        ));
    }

    /**
     * Creates a new sageFemme entity.
     *
     * @Route("/new", name="sagefemme_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $sageFemme = new Sagefemme();
        $form = $this->createForm('UserBundle\Form\SageFemmeType', $sageFemme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($sageFemme->getAge() === null && $sageFemme->getDateNaissance() !== null) {
                $today= new \DateTime('now');
                $dateOfBirth= $sageFemme->getDateNaissance();
                $age= date_diff(date_create($dateOfBirth->format('Y-m-d')), date_create($today->format('Y-m-d')));

                $sageFemme->setAge((int)$age->format('%y'));
            }
            $sageFemme->setLibUnique("sage_femme");
            $em->persist($sageFemme);
            $em->flush();
            $this->addFlash("success", "Enregistrement éffectué avec succès");
            return $this->redirectToRoute('sagefemme_show', array('id' => $sageFemme->getId()));
        }

        return $this->render('sagefemme/new.html.twig', array(
            'sageFemme' => $sageFemme,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a sageFemme entity.
     *
     * @Route("/{id}", name="sagefemme_show")
     * @Method("GET")
     */
    public function showAction(SageFemme $sageFemme)
    {
        $deleteForm = $this->createDeleteForm($sageFemme);

        return $this->render('sagefemme/show.html.twig', array(
            'autreEmploye' => $sageFemme,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing sageFemme entity.
     *
     * @Route("/{id}/edit", name="sagefemme_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, SageFemme $sageFemme)
    {
        $deleteForm = $this->createDeleteForm($sageFemme);
        $editForm = $this->createForm('UserBundle\Form\SageFemmeType', $sageFemme);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($sageFemme->getAge() === null && $sageFemme->getDateNaissance() !== null) {
                $today= new \DateTime('now');
                $dateOfBirth= $sageFemme->getDateNaissance();
                $age= date_diff(date_create($dateOfBirth->format('Y-m-d')), date_create($today->format('Y-m-d')));

                $sageFemme->setAge((int)$age->format('%y'));
            }
            $em->flush();
            $this->addFlash("success", "Modification éffectuée avec succès");
            return $this->redirectToRoute('sagefemme_edit', array('id' => $sageFemme->getId()));
        }

        return $this->render('sagefemme/edit.html.twig', array(
            'sageFemme' => $sageFemme,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a sageFemme entity.
     *
     * @Route("/{id}", name="sagefemme_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, SageFemme $sageFemme)
    {
        $form = $this->createDeleteForm($sageFemme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sageFemme);
            $em->flush();
        }

        return $this->redirectToRoute('sagefemme_index');
    }

    /**
     * Creates a form to delete a sageFemme entity.
     *
     * @param SageFemme $sageFemme The sageFemme entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SageFemme $sageFemme)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sagefemme_delete', array('id' => $sageFemme->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
