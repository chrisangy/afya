<?php

namespace App\Controller\StockBundle\Controller;

use StockBundle\Entity\CategorieArticle;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Categoriearticle controller.
 *
 * @Route("categorie/article")
 */
class CategorieArticleController extends AbstractController
{
    /**
     * Lists all categorieArticle entities.
     *
     * @Route("/", name="categorie_article_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categorieArticles = $em->getRepository('StockBundle:CategorieArticle')->findAll();

        return $this->render('categoriearticle/index.html.twig', array(
            'categorieArticles' => $categorieArticles,
        ));
    }

    /**
     * Creates a new categorieArticle entity.
     *
     * @Route("/new", name="categorie_article_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $categorieArticle = new Categoriearticle();
        $form = $this->createForm('StockBundle\Form\CategorieArticleType', $categorieArticle);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $categorieArticle->setCreatedAt(new \DateTime())
                ->setDeleted(false);
            $em->persist($categorieArticle);
            $em->flush();
            $this->addFlash("success", "Enregistrement éffectué avec succès");
            return $this->redirectToRoute('categorie_article_index' );
        }

        return $this->render('categoriearticle/new.html.twig', array(
            'categorieArticle' => $categorieArticle,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing categorieArticle entity.
     *
     * @Route("/{reference}/edit", name="categorie_article_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, CategorieArticle $categorieArticle)
    {
        $editForm = $this->createForm('StockBundle\Form\CategorieArticleType', $categorieArticle);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", "Modification éffectuée avec succès");
            return $this->redirectToRoute('categorie_article_index');
        }

        return $this->render('categoriearticle/edit.html.twig', array(
            'categorieArticle' => $categorieArticle,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a categorieArticle entity.
     *
     * @Route("/supprimer/{reference}", name="categorie_article_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, CategorieArticle $categorieArticle)
    {
        $form = $this->createDeleteForm($categorieArticle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($categorieArticle);
            $em->flush();
        }
        $this->addFlash("success", "Enregistrement effacé avec succès");
        return $this->redirectToRoute('categorie_article_index');
    }

    /**
     * Creates a form to delete a categorieArticle entity.
     *
     * @param CategorieArticle $categorieArticle The categorieArticle entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CategorieArticle $categorieArticle)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('categorie_article_delete', array('id' => $categorieArticle->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
