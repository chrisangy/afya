<?php

namespace App\Controller\StockBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use StockBundle\Entity\Entree;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Entree controller.
 *
 * @Route("entree")
 */
class EntreeController extends AbstractController
{
    /**
     * Lists all entree entities.
     *
     * @Route("/", name="entree_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entrees = $em->getRepository('StockBundle:Entree')->findAll();

        return $this->render('entree/index.html.twig', array(
            'entrees' => $entrees,
        ));
    }

    /**
     * Creates a new entree entity.
     *
     * @Route("/new", name="entree_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $entree = new Entree();
        $form = $this->createForm('StockBundle\Form\EntreeType', $entree,[
            'action' => $this->generateUrl('entree_new'),
            'method' => 'POST',
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //  mise a jour de la quantité

            $em = $this->getDoctrine()->getManager();
            $entree->getArticle()->setQteStock($entree->getArticle()->getQteStock() + $entree->getQuantite());
            $em->persist($entree);
            $em->flush();
            $this->addFlash("success", "Enregistrement éffectué avec succès");
            return $this->redirectToRoute('entree_index');
        }

        return $this->render('entree/new.html.twig', array(
            'entree' => $entree,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a entree entity.
     *
     * @Route("/{id}", name="entree_show")
     * @Method("GET")
     */
    public function showAction(Entree $entree)
    {
        $deleteForm = $this->createDeleteForm($entree);

        return $this->render('entree/show.html.twig', array(
            'entree' => $entree,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing entree entity.
     *
     * @Route("/{id}/edit", name="entree_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Entree $entree)
    {
        $deleteForm = $this->createDeleteForm($entree);
        $editForm = $this->createForm('StockBundle\Form\EntreeType', $entree);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", "Modification éffectuée avec succès");
            return $this->redirectToRoute('entree_edit', array('id' => $entree->getId()));
        }

        return $this->render('entree/edit.html.twig', array(
            'entree' => $entree,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a entree entity.
     *
     * @Route("/{id}", name="entree_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Entree $entree)
    {
        $form = $this->createDeleteForm($entree);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entree);
            $em->flush();
        }
        $this->addFlash("success", "Enregistrement effacé avec succès");
        return $this->redirectToRoute('entree_index');
    }

    /**
     * Creates a form to delete a entree entity.
     *
     * @param Entree $entree The entree entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Entree $entree)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('entree_delete', array('id' => $entree->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
