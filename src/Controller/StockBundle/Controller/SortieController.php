<?php

namespace App\Controller\StockBundle\Controller;

use StockBundle\Entity\Sortie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Sortie controller.
 *
 * @Route("sortie")
 */
class SortieController extends AbstractController
{
    /**
     * Lists all sortie entities.
     *
     * @Route("/", name="sortie_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sorties = $em->getRepository('StockBundle:Sortie')->findAll();

        return $this->render('sortie/index.html.twig', array(
            'sorties' => $sorties,
        ));
    }

    /**
     * Creates a new sortie entity.
     *
     * @Route("/new", name="sortie_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $sortie = new Sortie();
        $form = $this->createForm('StockBundle\Form\SortieType', $sortie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sortie);
            $sortie->getArticle()->setQteStock($sortie->getArticle()->getQteStock() - $sortie->getQuantite());
            $em->flush();
            $this->addFlash("success", "Enregistrement éffectué avec succès");
            return $this->redirectToRoute('sortie_index', array('id' => $sortie->getId()));
        }

        return $this->render('sortie/new.html.twig', array(
            'sortie' => $sortie,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a sortie entity.
     *
     * @Route("/{id}", name="sortie_show")
     * @Method("GET")
     */
    public function showAction(Sortie $sortie)
    {
        $deleteForm = $this->createDeleteForm($sortie);

        return $this->render('sortie/show.html.twig', array(
            'sortie' => $sortie,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing sortie entity.
     *
     * @Route("/{id}/edit", name="sortie_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Sortie $sortie)
    {
        $deleteForm = $this->createDeleteForm($sortie);
        $editForm = $this->createForm('StockBundle\Form\SortieType', $sortie);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", "Modification éffectuée avec succès");
            return $this->redirectToRoute('sortie_edit', array('id' => $sortie->getId()));
        }

        return $this->render('sortie/edit.html.twig', array(
            'sortie' => $sortie,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a sortie entity.
     *
     * @Route("/{id}", name="sortie_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Sortie $sortie)
    {
        $form = $this->createDeleteForm($sortie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sortie);
            $em->flush();
        }
        $this->addFlash("success", "Enregistrement effacé avec succès");
        return $this->redirectToRoute('sortie_index');
    }

    /**
     * Creates a form to delete a sortie entity.
     *
     * @param Sortie $sortie The sortie entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Sortie $sortie)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sortie_delete', array('id' => $sortie->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
