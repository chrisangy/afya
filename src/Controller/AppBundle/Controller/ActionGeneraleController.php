<?php

namespace App\Controller\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ActionGeneraleController extends AbstractController
{
    /**
     * @Route("/all/rendezvous",name="all_rendezvous")
     */
    public function mailAction()
    {
        $sujet = "Erreur lors de l'envoi des comptes ";
        $receveur = 'renaudstrifler@gmail.com';
        $rep = $this->get('app.mail')->sendMail($sujet, $receveur);

        return $this->render('mail/errormail.html.twig');
    }
}
