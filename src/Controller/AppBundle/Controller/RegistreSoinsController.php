<?php

namespace App\Controller\AppBundle\Controller;

use AppBundle\Entity\RegistreSoins;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\Personne;

/**
 * Registresoin controller.
 *
 * @Route("registre/soins")
 */
class RegistreSoinsController extends AbstractController
{


    /**
     * Lists all registreSoin entities.
     *
     * @Route("/", name="registresoins_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $registreSoins = $em->getRepository('AppBundle:RegistreSoins')->findAll();

        return $this->render('registresoins/index.html.twig', array(
            'registreSoins' => $registreSoins,
        ));
    }

    /**
     * Creates a new registreSoin entity.
     *
     * @Route("/new", name="registresoins_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $registreSoin = new RegistreSoins();
        $form = $this->createForm('AppBundle\Form\RegistreSoinsType', $registreSoin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $personId = $request->request->get('appbundle_registresoins')['patient'];
            $personne = $em->getRepository(Personne::class)->find($personId);

            $registreSoin->setPatient($personne);
            $registreSoin->setAge($personne->getAge());
            $registreSoin->setSexe($personne->getSexe());
            $em->persist($registreSoin);
            $em->flush();
            $this->addFlash("success", 'Creation éffectuée avec succès');
            return $this->redirectToRoute('registresoins_index');
//            return $this->redirectToRoute('registresoins_show', array('id' => $registreSoin->getId()));
        }

        return $this->render('registresoins/new.html.twig', array(
            'registreSoin' => $registreSoin,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new registreSoin entity.
     *
     * @Route("/nouveau/registre_de_soins/{matricule}", name="registresoins_new_redirect")
     * @Method({"GET", "POST"})
     */
    public function newRedirectAction(Personne $personne, Request $request)
    {
        $registreSoin = new RegistreSoins();
        $form = $this->createForm('AppBundle\Form\RegistreSoinsType', $registreSoin);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
//            $personId = $request->request->get('appbundle_registresoins')['patient'];
//            $personneSelected = $em->getRepository(Personne::class)->find($personId);
            $registreSoin->setPatient($personne);
            $registreSoin->setAge($personne->getAge());
            $registreSoin->setSexe($personne->getSexe());
            $em->persist($registreSoin);
            $em->flush();
            $this->addFlash("success", 'Creation éffectuée avec succès');
            return $this->redirectToRoute('registresoins_index');
//            return $this->redirectToRoute('registresoins_show', array('id' => $registreSoin->getId()));
        }

        return $this->render('registresoins/new.html.twig', array(
            'registreSoin' => $registreSoin,
            'form' => $form->createView(),
            'personne' => $personne,
        ));
    }

    /**
     * Finds and displays a registreSoin entity.
     *
     * @Route("/{id}", name="registresoins_show")
     * @Method("GET")
     */
    public function showAction(RegistreSoins $registreSoin)
    {
        $deleteForm = $this->createDeleteForm($registreSoin);

        return $this->render('registresoins/show.html.twig', array(
            'registreSoin' => $registreSoin,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing registreSoin entity.
     *
     * @Route("/{id}/edit", name="registresoins_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, RegistreSoins $registreSoin)
    {
        $editForm = $this->createForm('AppBundle\Form\RegistreSoinsType', $registreSoin);
        $editForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
                $personId = $request->request->get('appbundle_registresoins')['patient'];
            $personne = $em->getRepository(Personne::class)->find($personId);

            $registreSoin->setPatient($personne);
            $registreSoin->setAge($personne->getAge());
            $registreSoin->setSexe($personne->getSexe());


            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", 'Modification éffectuée avec succès');
            return $this->redirectToRoute('registresoins_index');
        }

        return $this->render('registresoins/edit.html.twig', array(
            'registreSoin' => $registreSoin,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a registreSoin entity.
     *
     * @Route("/{id}", name="registresoins_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, RegistreSoins $registreSoin)
    {
        $form = $this->createDeleteForm($registreSoin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($registreSoin);
            $em->flush();
        }

        return $this->redirectToRoute('registresoins_index');
    }

    /**
     * Creates a form to delete a registreSoin entity.
     *
     * @param RegistreSoins $registreSoin The registreSoin entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(RegistreSoins $registreSoin)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('registresoins_delete', array('id' => $registreSoin->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
