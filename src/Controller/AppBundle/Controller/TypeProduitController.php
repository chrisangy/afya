<?php

namespace App\Controller\AppBundle\Controller;

use AppBundle\Entity\TypeProduit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Typeproduit controller.
 *
 * @Route("typeproduit")
 */
class TypeProduitController extends AbstractController
{
    /**
     * Lists all typeProduit entities.
     *
     * @Route("/", name="typeproduit_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $typeProduits = $em->getRepository('AppBundle:TypeProduit')->findAll();

        return $this->render('typeproduit/index.html.twig', array(
            'typeProduits' => $typeProduits,
        ));
    }

    /**
     * Creates a new typeProduit entity.
     *
     * @Route("/new", name="typeproduit_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $typeProduit = new Typeproduit();
        $form = $this->createForm('AppBundle\Form\TypeProduitType', $typeProduit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typeProduit);
            $em->flush();
            $message = 'Produit cree avec success';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('typeproduit_index');
        }

        return $this->render('typeproduit/new.html.twig', array(
            'typeProduit' => $typeProduit,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a typeProduit entity.
     *
     * @Route("/{id}", name="typeproduit_show")
     * @Method("GET")
     */
    public function showAction(TypeProduit $typeProduit)
    {
        $deleteForm = $this->createDeleteForm($typeProduit);

        return $this->render('typeproduit/show.html.twig', array(
            'typeProduit' => $typeProduit,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing typeProduit entity.
     *
     * @Route("/{id}/edit", name="typeproduit_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TypeProduit $typeProduit)
    {
        $deleteForm = $this->createDeleteForm($typeProduit);
        $editForm = $this->createForm('AppBundle\Form\TypeProduitType', $typeProduit);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('typeproduit_edit', array('id' => $typeProduit->getId()));
        }

        return $this->render('typeproduit/edit.html.twig', array(
            'typeProduit' => $typeProduit,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a typeProduit entity.
     *
     * @Route("/{id}", name="typeproduit_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TypeProduit $typeProduit)
    {
        $form = $this->createDeleteForm($typeProduit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typeProduit);
            $em->flush();
        }

        return $this->redirectToRoute('typeproduit_index');
    }

    /**
     * Creates a form to delete a typeProduit entity.
     *
     * @param TypeProduit $typeProduit The typeProduit entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TypeProduit $typeProduit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typeproduit_delete', array('id' => $typeProduit->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
