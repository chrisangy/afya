<?php

namespace App\Controller\AppBundle\Controller;

use AppBundle\Entity\RegistreSoins;
use AppBundle\Form\Model\PeriodeSearchType;
use AppBundle\Model\PeriodSearch;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Impression controller.
 *
 * @Route("impression")
 */
class ImpressionController extends AbstractController
{

    /**
     * index of statistiques
     *
     * @Route("/statitique/soins", name="statitique_soins_index")
     * @Method("GET")
     */
    public function statistiqueAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

//        $searchAbscence = new PeriodSearch();
//        $form = $this->createForm(PeriodeSearchType::class, $searchAbscence);
//        $form->handleRequest($request);

        if ($request->isMethod('POST')) {

            $affectations = $em->getRepository('AppBundle:Affections')->findAll();
            $superArray = [];


            foreach ($affectations as $affectation) {
                //recuperer les soins durant la periode selectionnée


//                $dateDebut = $searchAbscence->getDateDebut();
//                $dateDebut = $dateDebut->format('d-m-Y');
//
//                $dateFin = $searchAbscence->getDateFin();
//                $dateFin = $dateFin->format('d-m-Y');
                $mois = $request->get('mois');
                $annees = $request->get('annees');
                $query_date = $annees . '-' . $mois . '-01';
                $dateDebut = new \DateTime(date('Y-m-01', strtotime($query_date)));
                $dateFin = new \DateTime(date('Y-m-t', strtotime($query_date)));
//                dump($dateDebut->format('Y-m-d'),$dateFin->format('Y-m-d'));die();
                $soins = $em->getRepository('AppBundle:RegistreSoins')->findPeriodeSoin($dateDebut->format('Y-m-d'), $dateFin->format('Y-m-d'), $affectation);

                $detailData = ['M' => 0, 'F' => 0];
                $superArray[$affectation->getDesignation()]['deces'] = ['hospitalize' => [], 'unhospitalize' => []];
                $superArray[$affectation->getDesignation()]['cas'] = ['hospitalize' => [], 'unhospitalize' => []];

                $this->get('impression_service')->createUsageArray($superArray[$affectation->getDesignation()]['deces']['hospitalize']);
                $this->get('impression_service')->createUsageArray($superArray[$affectation->getDesignation()]['cas']['hospitalize']);
                $this->get('impression_service')->createUsageArray($superArray[$affectation->getDesignation()]['deces']['unhospitalize']);
                $this->get('impression_service')->createUsageArray($superArray[$affectation->getDesignation()]['cas']['unhospitalize']);


                foreach ($soins as $soin) {
                    $this->generateHealthStat(0, 0.1, false, $soin, $superArray);
                    $this->generateHealthStat(0.1, 0.11, false, $soin, $superArray);
                    $this->generateHealthStat(1, 4, false, $soin, $superArray);
                    $this->generateHealthStat(5, 9, false, $soin, $superArray);
                    $this->generateHealthStat(10, 14, false, $soin, $superArray);
                    $this->generateHealthStat(15, 19, false, $soin, $superArray);
                    $this->generateHealthStat(20, 24, false, $soin, $superArray);
                    $this->generateHealthStat(25, 1000, false, $soin, $superArray);
                }

            }

            $render = $this->renderView('pdfFiles/statistiqueGenerale.html.twig', ['datas' => $superArray, 'datedebut' => $dateDebut->format('Y-m-d'), 'datefin' => $dateFin->format('Y-m-d')]);
//            return $this->render('pdfFiles/statistiqueGenerale.html.twig', ['datas' => $superArray]);
            return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($render, array('orientation' => 'landscape')),
                200,
                array(
                    'Content-Type' => 'application/pdf',
                    'orientation' => 'Landscape',
                    'Content-Disposition' => 'attachment; filename= statistique generale.pdf'
                )
            );
//            return $this->rexnder('pdfFiles/statistiqueGenerale.html.twig', ['datas' => $superArray]);
        }
        return $this->render('impression/index/statistiqueIndex.html.twig');
    }

    public function getCurrentArrayIndex($minAge, $maxAge)
    {
        $currentIndex = "01";
        if ($minAge >= 0 && $maxAge >= 0.1) {
            $currentIndex = "01";
        }
        if ($minAge >= 0.1 && $maxAge >= 0.11) {
            $currentIndex = "111";
        }
        if ($minAge >= 1 && $maxAge >= 4) {
            $currentIndex = "14";
        }
        if ($minAge >= 5 && $maxAge >= 9) {
            $currentIndex = "59";
        }
        if ($minAge >= 10 && $maxAge >= 14) {
            $currentIndex = "1014";
        }
        if ($minAge >= 15 && $maxAge >= 19) {
            $currentIndex = "1519";
        }
        if ($minAge >= 20 && $maxAge >= 24) {
            $currentIndex = "2024";
        }
        if ($minAge >= 25 && $maxAge >= 2000) {
            $currentIndex = "25";
        }
        return $currentIndex;
    }

    /**
     * @param $minAge
     * @param $maxAge
     * @param bool $hospitalized
     * @param RegistreSoins $soin
     * @param $superArray
     */
    public function generateHealthStat($minAge, $maxAge, $hospitalized = false, RegistreSoins $soin, &$superArray)
    {

        if ($soin->getAge() >= $minAge && $soin->getAge() <= $maxAge) {
            $currentSuperArrayIndex = $this->getCurrentArrayIndex($minAge, $maxAge);
            dump($currentSuperArrayIndex);
            if ($soin->getSexe() == "M") {
                if ($soin->getEstHospitalise()) {
                    if ($soin->getEstMort()) {
                        $lastMenDieCaseCount = $superArray[$soin->getDiagnostique()->getDesignation()]['deces']['hospitalize'][$currentSuperArrayIndex]['M'];
                        $superArray[$soin->getDiagnostique()->getDesignation()]['deces']['hospitalize'][$currentSuperArrayIndex]['M'] = $lastMenDieCaseCount + 1;

                    } else {
                        $lastMenNormalCaseCount = $superArray[$soin->getDiagnostique()->getDesignation()]['cas']['hospitalize'][$currentSuperArrayIndex]['M'];
                        $superArray[$soin->getDiagnostique()->getDesignation()]['cas']['hospitalize'][$currentSuperArrayIndex]['M'] = $lastMenNormalCaseCount + 1;
                    }
                } else {

                    if ($soin->getEstMort()) {
                        $lastMenDieCaseCount = $superArray[$soin->getDiagnostique()->getDesignation()]['deces']['unhospitalize'][$currentSuperArrayIndex]['M'];
                        $superArray[$soin->getDiagnostique()->getDesignation()]['deces']['unhospitalize'][$currentSuperArrayIndex]['M'] = $lastMenDieCaseCount + 1;

                    } else {
                        $lastMenNormalCaseCount = $superArray[$soin->getDiagnostique()->getDesignation()]['cas']['unhospitalize'][$currentSuperArrayIndex]['M'];
                        $superArray[$soin->getDiagnostique()->getDesignation()]['cas']['unhospitalize'][$currentSuperArrayIndex]['M'] = $lastMenNormalCaseCount + 1;

                    }
                }

            } else {

                if ($soin->getEstHospitalise()) {
                    if ($soin->getEstMort()) {
                        $lastWomenDieCaseCount = $superArray[$soin->getDiagnostique()->getDesignation()]['deces']['hospitalize'][$currentSuperArrayIndex]['F'];
                        $superArray[$soin->getDiagnostique()->getDesignation()]['deces']['hospitalize'][$currentSuperArrayIndex]['F'] = $lastWomenDieCaseCount + 1;

                    } else {
                        $lastWomenNormalCaseCount = $superArray[$soin->getDiagnostique()->getDesignation()]['cas']['hospitalize'][$currentSuperArrayIndex]['F'];
                        $superArray[$soin->getDiagnostique()->getDesignation()]['cas']['hospitalize'][$currentSuperArrayIndex]['F'] = $lastWomenNormalCaseCount + 1;
                    }
                } else {

                    if ($soin->getEstMort()) {
                        $lastWomenDieCaseCount = $superArray[$soin->getDiagnostique()->getDesignation()]['deces']['unhospitalize'][$currentSuperArrayIndex]['F'];
                        $superArray[$soin->getDiagnostique()->getDesignation()]['deces']['unhospitalize'][$currentSuperArrayIndex]['F'] = $lastWomenDieCaseCount + 1;

                    } else {
                        $lastWomenNormalCaseCount = $superArray[$soin->getDiagnostique()->getDesignation()]['cas']['unhospitalize'][$currentSuperArrayIndex]['F'];
                        $superArray[$soin->getDiagnostique()->getDesignation()]['cas']['unhospitalize'][$currentSuperArrayIndex]['F'] = $lastWomenNormalCaseCount + 1;
                    }
                }


            }
        }
    }

}
