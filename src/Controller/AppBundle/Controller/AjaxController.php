<?php

namespace App\Controller\AppBundle\Controller;

use AppBundle\Entity\Consultation;
use AppBundle\Entity\ConsultationDonneSuivi;
use AppBundle\Entity\DonneSuivi;
use AppBundle\Entity\Dossier;
use AppBundle\Entity\Operation;
use AppBundle\Entity\TypeDonne;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use StockBundle\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\Personne;
use UserBundle\Entity\Patient;

class AjaxController extends AbstractController
{
    /**
     * @Route("/searchpatient", name="searchpatient", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function searchpatientAction(Request $request)
    {
        $fullname = $request->get('fullname');

        $em = $this->getDoctrine()->getManager();
        $tel = $request->get('tel');
        $liste = $em->getRepository(Personne::class)->searchUser($fullname, $tel);
//dump($liste);die();
        //        dump($fullname,$tel,$liste);
        //        die;
        return $this->container->get('templating')->renderResponse('patient/include/tableau.html.twig', array(
            'patients' => $liste,
        ));
    }

    /**
     * @Route("/get/product/price", name="ajax_product_price", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function getProductPrice(Request $request)
    {
        $productId = $request->get('product_id');

        $em = $this->getDoctrine()->getManager();

        $productObj = $em->getRepository(Article::class)->findOneBy(['id' => $productId]);


        return new Response($productObj->getPrixVente());
    }


    /**
     * @Route("/getAllRdv", name="getAllRdv", options= {"expose" = true})
     * @Method({"POST"})
     * @throws \Twig\Error\Error
     */
    public function getAllRdv(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $rendezVouses = $em->getRepository('AppBundle:RendezVous')->findBy(['estConsumer' => 0, 'estValider' => 1, "estAnnuler" => 0]);
        $rdvArry = [];
        foreach ($rendezVouses as $event) {
            $today = new \DateTime('now');
            $today_time = strtotime($today->format('Y-m-d'));
            $expire_time = strtotime($event->getDateDemande()->format('Y-m-d'));

            if ($expire_time < $today_time) {
                $event->setEstAnnuler(1);
                $em->persist($event);
            }
            $rdvArry[] = [
                "title" => $event->getPatient()->getFullname(),
                "start" => $event->getDateDemande()->format('Y-m-d'),
                "url" => $event->getId() . '/edit'
            ];
        }
        return $this->container->get('templating')->renderResponse('default/includes/dynamicView.html.twig', array(
            'rdv' => json_encode($rdvArry),
        ));
    }


    /**
     * @Route("/get/filter/operation", name="ajax_filtre_operation", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function getPeriodiqueOperation(Request $request)
    {
        $dateDebut = $request->get('dateDebut');
        $dateFin = $request->get('dateFin');
        $user = $request->get('user');

        $em = $this->getDoctrine()->getManager();

        $operations = $em->getRepository(Operation::class)->findPeriodiqueOperation($dateDebut, $dateFin);

        return $this->container->get('templating')->renderResponse('operation/include/tableau.html.twig', array(
            'operations' => $operations,
        ));
    }

    /**
     * @Route("/searchpatientinfo", name="searchpatientinfo", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function searchpatientinfoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $keyword = $request->query->get('term')['term'];
        $result = $em->getRepository(Patient::class)->searchPetientByNameOrMatriculeSelect2($keyword);
        $data = ['items' => $result];
        return new JsonResponse($data);
    }

    /**
     * @Route("/searchpatientdossierinfo", name="searchpatientdossierinfo", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function searchpatientdossierinfoAction(Request $request)
    {
        $keyword = $request->query->get('term')['term'];
        $em = $this->getDoctrine()->getManager();
        $result = [];
        // $patient = $em->getRepository(Patient::class)->searchPetientByNameOrMatricule($keyword);
        $dossier = $em->getRepository(Dossier::class)->searchPetientByNameOrMatricule($keyword);
        $data = ['items' => $dossier];
        return new JsonResponse($data);
    }

    /**
     * @Route("/searchdonnesuivi", name="searchdonnesuivi", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function searchdonnesuivi(Request $request)
    {
        $typeDonneRef = $request->query->get('typeDonne');
        $keyword = $request->query->get('term')['term'];
        $em = $this->getDoctrine()->getManager();

        $typeDonne = $em->getRepository(TypeDonne::class)->findOneBy(['reference' => $typeDonneRef]);
        $result = $em->getRepository(DonneSuivi::class)->findByTypeDonneRef($typeDonne, $keyword);

        $data = ['items' => $result];
        return new JsonResponse($data);
    }

    /**
     * @Route("/getenfantTypeDonne", name="getenfantTypeDonne", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function getenfantTypeDonne(Request $request)
    {
        $typeDonneRef = $request->get('typeDonne');
        $em = $this->getDoctrine()->getManager();

        $typeDonne = $em->getRepository(TypeDonne::class)->findOneBy(['reference' => $typeDonneRef, 'deleted' => false]);
        $lesEnfant = $em->getRepository(TypeDonne::class)->findBy(['parent' => $typeDonne, 'deleted' => false]);
        return $this->render('donnesuivi/_partial/_enfantTypeDonne.html.twig', [
            'lesEnfant' => $lesEnfant
        ]);
    }

    /**
     * @Route("/supprimerUnDonne", name="supprimerUnDonne", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function supprimerUnDonne(Request $request)
    {
        $codeConsultation = $request->get('codeConsultation');
        $donne = $request->get('donne');
        $em = $this->getDoctrine()->getManager();
        $donneSuivi = $em->getRepository(DonneSuivi::class)->findOneBy(['deleted' => 0, 'reference' => $donne]);
        $consulation = $em->getRepository(Consultation::class)->findOneBy(['reference' => $codeConsultation, 'deleted' => false]);
        $resulta = $em->getRepository(ConsultationDonneSuivi::class)->findOneBy(['consultation' => $consulation, 'donnesuivi' => $donneSuivi, 'deleted' => false]);
        $em->remove($resulta);
        $em->flush();
        return new Response('ok');
    }
}
