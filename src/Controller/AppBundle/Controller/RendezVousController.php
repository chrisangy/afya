<?php

namespace App\Controller\AppBundle\Controller;

use AppBundle\Entity\RendezVous;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Rendezvous controller.
 *
 * @Route("rendezvous")
 */
class RendezVousController extends AbstractController
{
    /**
     * Lists all rendezVous entities.
     *
     * @Route("/", name="rendezvous_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rendezVouses = $em->getRepository('AppBundle:RendezVous')->findBy(['estConsumer' => 0, 'estValider' => 1, "estAnnuler" => 0]);
        $rdVAnnuler = $em->getRepository('AppBundle:RendezVous')->findBy(['estConsumer' => 0, 'estValider' => 1, "estAnnuler" => 1]);
        $rendezVousNonValide = count($em->getRepository('AppBundle:RendezVous')->findBy(['estConsumer' => 0, 'estValider' => 0]));
        $today = new \DateTime('now');
        $rdvArry = [];
//        dump($rdVAnnuler);die();

        foreach ($rendezVouses as $event) {
            $today_time = strtotime($today->format('Y-m-d'));
            $expire_time = strtotime($event->getDateDemande()->format('Y-m-d'));

            if ($expire_time < $today_time) {
                $event->setEstAnnuler(1);
                $em->persist($event);
            }
            $rdvArry[] = [
                "title" => $event->getPatient()->getFullname(),
                "start" => $event->getDateDemande()->format('Y-m-d'),
                "url" => $event->getId() . '/edit'
            ];
        }
        $em->flush();

        return $this->render('rendezvous/index.html.twig', array(
            'rendezVouses' => $rendezVouses,
            'rendezVousNonValide' => $rendezVousNonValide,
            'rendezVousAnnule' => $rdVAnnuler,
            'rdv' => json_encode($rdvArry),
        ));
    }

    /**
     * Lists all rendezVous entities.
     *
     * @Route("/validation", name="validation_rendezvous")
     * @Method("GET")
     */
    public function validationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $rendezVouses = $em->getRepository('AppBundle:RendezVous')->findBy(['estConsumer' => 0, 'estValider' => 0]);

        if ($request->isMethod("POST")) {

            $rendezVousValide = $request->get('rendezVousValider');
            if (!empty($rendezVousValide)) {
                foreach ($rendezVousValide as $value) {

                    $currentRendezVouses = $em->getRepository('AppBundle:RendezVous')->findOneBy(['id' => $value]);

                    $currentRendezVouses->setEstValider(true);

                    $em->persist($currentRendezVouses);
                }

                $em->flush();

                $this->addFlash("success", 'Validation éffectée avec succès');
                return $this->redirectToRoute("validation_rendezvous");

            } else {
                $this->addFlash("warning", "Vueillez cocher les rendez vous avant de soumettre la requete");
            }


        }

        return $this->render('rendezvous/validation.html.twig', array(
            'rendezVouses' => $rendezVouses,
        ));
    }


    /**
     * Creates a new rendezVous entity.
     *
     * @Route("/new", name="rendezvous_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $rendezVous = new RendezVous();
        $form = $this->createForm('AppBundle\Form\RendezVousType', $rendezVous/*, array('csrf_protection' => false)*/);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            setlocale(LC_TIME, "fr_FR");
            $dateRendezVous = $form->getData()->getDateDemande();


            $unixTimestamp = strtotime($dateRendezVous->format('d-m-Y'));
            $unixTime = strtotime($dateRendezVous->format('H:i'));
            $dayOfWeek = date("l", $unixTimestamp);
            $frenchDay = self::getfrenchDay($dayOfWeek);

            $rendezVous->setJour($frenchDay);
//            ->setHeure($unixTime);


            $em = $this->getDoctrine()->getManager();
            $em->persist($rendezVous);
            $em->flush();
            $this->addFlash("success", "Enregistrement éffectué avec succès");
            return $this->redirectToRoute('rendezvous_index');
        }

        return $this->render('rendezvous/new.html.twig', array(
            'rendezVous' => $rendezVous,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new rendezVous entity.
     *
     * @Route("/new/test", name="rendezvous_new_test")
     * @Method({"GET", "POST"})
     */
    public function newTestEventAction(Request $request)
    {
        $rendezVous = new RendezVous();
        $form = $this->createForm('AppBundle\Form\RendezVousType', $rendezVous);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            setlocale(LC_TIME, "fr_FR");
            $dateRendezVous = $form->getData()->getDateDemande();

            $unixTimestamp = strtotime($dateRendezVous->format('d-m-Y'));
            $unixTime = strtotime($dateRendezVous->format('H:i'));
            $dayOfWeek = date("l", $unixTimestamp);
            $frenchDay = self::getfrenchDay($dayOfWeek);

            $rendezVous->setJour($frenchDay)
                ->setHeure($unixTime);


            $em = $this->getDoctrine()->getManager();
            $em->persist($rendezVous);
            $em->flush();
            $this->addFlash("success", "Enregistrement éffectué avec succès");
            return $this->redirectToRoute('rendezvous_index');
        }

        return $this->render('rendezvous/newtest.html.twig', array(
            'rendezVous' => $rendezVous,
            'form' => $form->createView(),
        ));
    }


    /**
     * Creates a new rendezVous entity.
     *
     * @Route("/annuler/{id}", name="rendezvous_annulation")
     * @Method({"GET"})
     */
    public function anullerRendezVous(Request $request, RendezVous $rendezVous)
    {

        $rendezVous->setEstAnnuler(1);


        $this->getDoctrine()->getManager()->flush();

        $this->addFlash("success", " Annulation de rendez-vous effectuée avec succès");

        return $this->redirectToRoute('rendezvous_index');

    }

    /**
     * Finds and displays a rendezVous entity.
     *
     * @Route("/{id}", name="rendezvous_show")
     * @Method("GET")
     */
    public function showAction(RendezVous $rendezVous)
    {
        $deleteForm = $this->createDeleteForm($rendezVous);

        return $this->render('rendezvous/show.html.twig', array(
            'rendezVous' => $rendezVous,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing rendezVous entity.
     *
     * @Route("/{id}/edit", name="rendezvous_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, RendezVous $rendezVous)
    {
        $deleteForm = $this->createDeleteForm($rendezVous);
        $editForm = $this->createForm('AppBundle\Form\RendezVousType', $rendezVous);
        $editForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $today = new \DateTime('');
            $today_time = strtotime($today->format('Y-m-d'));
            $expire_time = strtotime($rendezVous->getDateDemande()->format('Y-m-d'));
            if ($expire_time > $today_time && $rendezVous->getEstAnnuler() == 1) {
                $rendezVous->setEstAnnuler(0);
            }
            $em->persist($rendezVous);
            $em->flush();
            $this->addFlash("success", "Modification éffectuée avec succès");
            return $this->redirectToRoute('rendezvous_index');
        }

        return $this->render('rendezvous/edit.html.twig', array(
            'rendezVous' => $rendezVous,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a rendezVous entity.
     *
     * @Route("/{id}", name="rendezvous_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, RendezVous $rendezVous)
    {
        $form = $this->createDeleteForm($rendezVous);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($rendezVous);
            $em->flush();
        }

        return $this->redirectToRoute('rendezvous_index');
    }

    /**
     * Creates a form to delete a rendezVous entity.
     *
     * @param RendezVous $rendezVous The rendezVous entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(RendezVous $rendezVous)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('rendezvous_delete', array('id' => $rendezVous->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    private function getfrenchDay($englishDay)
    {

        $frenchDay = '';


        switch (trim($englishDay)) {
            case 'Monday':
                $frenchDay = 'Lundi';
                break;
            case 'Tuesday':
                $frenchDay = 'Mardi';
                break;
            case 'Wednesday':
                $frenchDay = 'Mercredi';
                break;
            case 'Thursday':
                $frenchDay = 'Jeudi';
                break;
            case 'Friday':
                $frenchDay = 'Vendredi';
                break;
            case 'Saturday':
                $frenchDay = 'Samedi';
                break;
            case 'Sunday':
                $frenchDay = 'Dimanche';
                break;
            default:
                $frenchDay = '';

        }

        return $frenchDay;


    }
}
