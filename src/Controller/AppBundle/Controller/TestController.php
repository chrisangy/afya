<?php

namespace App\Controller\AppBundle\Controller;

use AppBundle\Entity\RegistreSoins;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class TestController extends AbstractController
{

    /**
     * @Route("/apropos",name="apropos")
     */
    public function aproposAction()
    {
        return $this->render('default/apropo.html.twig');
    }
    /**
     * @Route("/tespdf",name="testpdf")
     */
    public function testAction()
    {

        $sujet = "Erreur lors de l'envoi des comptes ";
        $receveur = 'renaudstrifler@gmail.com';
        $rep = $this->get('app.mail')->sendMail($sujet, $receveur);

        $render = $this->renderView('test.html.twig');

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($render),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename= test.pdf'
            )
        );
    }

    /**
     * @Route("/test/create/dossuer",name="testpdfdfaq")
     */
    public function createDossier()
    {

        $em = $this->getDoctrine()->getManager();


        $consultations = $em->getRepository('UserBundle:PatientStandard')->find(1);

        $ok = $this->get('dossier_service')->createDossier($consultations);

        dump($ok);
        die;
    }

    /**
     * @Route("/test/create/statistique",name="test.create.statistique")
     */
    public function createStatFiche1()
    {

        $em = $this->getDoctrine()->getManager();


            $affectations = $em->getRepository('AppBundle:Affections')->findAll();
        $superArray = [];

        foreach ($affectations as $affectation) {
            //recuperer les soins durant la periode selectionnée
            $soins = $em->getRepository('AppBundle:RegistreSoins')->findBy(['diagnostique' => $affectation]);


            $detailData = ['M' => 0, 'F' => 0];
            $superArray[$affectation->getDesignation()]['deces'] = ['hospitalize' => [], 'unhospitalize' => []];
            $superArray[$affectation->getDesignation()]['cas'] = ['hospitalize' => [], 'unhospitalize' => []];

            $this->createUageArray($superArray[$affectation->getDesignation()]['deces']['hospitalize']);
            $this->createUageArray($superArray[$affectation->getDesignation()]['cas']['hospitalize']);
            $this->createUageArray($superArray[$affectation->getDesignation()]['deces']['unhospitalize']);
            $this->createUageArray($superArray[$affectation->getDesignation()]['cas']['unhospitalize']);


            foreach ($soins as $soin) {
                $this->generateHealthStat(0, 0.1, false, $soin, $superArray);
                $this->generateHealthStat(0.1, 0.11, false, $soin, $superArray);
                $this->generateHealthStat(1, 4, false, $soin, $superArray);
                $this->generateHealthStat(5, 9, false, $soin, $superArray);
                $this->generateHealthStat(10, 14, false, $soin, $superArray);
                $this->generateHealthStat(15, 19, false, $soin, $superArray);
                $this->generateHealthStat(20, 24, false, $soin, $superArray);
                $this->generateHealthStat(25, 1000, false, $soin, $superArray);
            }

        }
//        dump($superArray);
//        die;
        return $this->render('pdfFiles/statistiqueGenerale.html.twig', ['datas' => $superArray]);


    }

    public function resetSuperArray(&$superArray, $maladieDesignation)
    {

        $superArray[$maladieDesignation] = [];
        $detailData = ['M' => 0, 'F' => 0];

        $superArray[$maladieDesignation]['cas'] = [];
        $superArray[$maladieDesignation]['deces'] = [];
    }


    public function createUageArray(&$arrayModified)
    {
        $detailData = ['M' => 0, 'F' => 0];
        if (!isset($arrayModified['01'])){
            $arrayModified['01'] =$detailData;
        }
        if (!isset($arrayModified['111'])){
            $arrayModified['111'] =$detailData;
        }
        if (!isset($arrayModified['14'])){
            $arrayModified['14'] =$detailData;
        }
        if (!isset($arrayModified['59'])){
            $arrayModified['59'] =$detailData;
        }
        if (!isset($arrayModified['1014'])){
            $arrayModified['1014'] =$detailData;
        }
        if (!isset($arrayModified['1519'])){
            $arrayModified['1519'] =$detailData;
        }
        if (!isset($arrayModified['2024'])){
            $arrayModified['2024'] =$detailData;
        }
        if (!isset($arrayModified['25'])){
            $arrayModified['25'] =$detailData;
        }


    }

    public function getCurrentArrayIndex($minAge, $maxAge)
    {
        $currentIndex = "01";
        if ($minAge >= 0 && $maxAge >= 0.1) {
            $currentIndex = "01";
        }
        if ($minAge >= 0.1 && $maxAge >= 0.11) {
            $currentIndex = "111";
        }
        if ($minAge >= 1 && $maxAge >= 4) {
            $currentIndex = "14";
        }
        if ($minAge >= 5 && $maxAge >= 9) {
            $currentIndex = "59";
        }
        if ($minAge >= 10 && $maxAge >= 14) {
            $currentIndex = "1014";
        }
        if ($minAge >= 15 && $maxAge >= 19) {
            $currentIndex = "1519";
        }
        if ($minAge >= 20 && $maxAge >= 24) {
            $currentIndex = "2024";
        }
        if ($minAge >= 25 && $maxAge >= 2000) {
            $currentIndex = "25";
        }
        return $currentIndex;
    }

    /**
     * @param $minAge
     * @param $maxAge
     * @param bool $hospitalized
     * @param RegistreSoins $soin
     * @param $superArray
     */
    public function generateHealthStat($minAge, $maxAge, $hospitalized = false, RegistreSoins $soin, &$superArray)
    {

        if ($soin->getAge() >= $minAge && $soin->getAge() <= $maxAge) {
            $currentSuperArrayIndex = $this->getCurrentArrayIndex($minAge, $maxAge);
            dump($currentSuperArrayIndex);
            if ($soin->getSexe() == "M") {
                if ($soin->getEstHospitalise()) {
                    if ($soin->getEstMort()) {
                        $lastMenDieCaseCount = $superArray[$soin->getDiagnostique()->getDesignation()]['deces']['hospitalize'][$currentSuperArrayIndex]['M'];
                        $superArray[$soin->getDiagnostique()->getDesignation()]['deces']['hospitalize'][$currentSuperArrayIndex]['M'] = $lastMenDieCaseCount + 1;

                    } else {
                        $lastMenNormalCaseCount = $superArray[$soin->getDiagnostique()->getDesignation()]['cas']['hospitalize'][$currentSuperArrayIndex]['M'];
                        $superArray[$soin->getDiagnostique()->getDesignation()]['cas']['hospitalize'][$currentSuperArrayIndex]['M'] = $lastMenNormalCaseCount + 1;
                    }
                }else{

                    if ($soin->getEstMort()) {
                        $lastMenDieCaseCount = $superArray[$soin->getDiagnostique()->getDesignation()]['deces']['unhospitalize'][$currentSuperArrayIndex]['M'];
                        $superArray[$soin->getDiagnostique()->getDesignation()]['deces']['unhospitalize'][$currentSuperArrayIndex]['M'] = $lastMenDieCaseCount + 1;

                    } else {
                        $lastMenNormalCaseCount = $superArray[$soin->getDiagnostique()->getDesignation()]['cas']['unhospitalize'][$currentSuperArrayIndex]['M'];
                        $superArray[$soin->getDiagnostique()->getDesignation()]['cas']['unhospitalize'][$currentSuperArrayIndex]['M'] = $lastMenNormalCaseCount + 1;

                    }
                }

            } else {

                if ($soin->getEstHospitalise()) {
                    if ($soin->getEstMort()) {
                        $lastWomenDieCaseCount = $superArray[$soin->getDiagnostique()->getDesignation()]['deces']['hospitalize'][$currentSuperArrayIndex]['F'];
                        $superArray[$soin->getDiagnostique()->getDesignation()]['deces']['hospitalize'][$currentSuperArrayIndex]['F'] = $lastWomenDieCaseCount + 1;

                    } else {
                        $lastWomenNormalCaseCount = $superArray[$soin->getDiagnostique()->getDesignation()]['cas']['hospitalize'][$currentSuperArrayIndex]['F'];
                        $superArray[$soin->getDiagnostique()->getDesignation()]['cas']['hospitalize'][$currentSuperArrayIndex]['F'] = $lastWomenNormalCaseCount + 1;
                    }
                } else{

                if ($soin->getEstMort()) {
                    $lastWomenDieCaseCount = $superArray[$soin->getDiagnostique()->getDesignation()]['deces']['unhospitalize'][$currentSuperArrayIndex]['F'];
                    $superArray[$soin->getDiagnostique()->getDesignation()]['deces']['unhospitalize'][$currentSuperArrayIndex]['F'] = $lastWomenDieCaseCount + 1;

                } else {
                    $lastWomenNormalCaseCount = $superArray[$soin->getDiagnostique()->getDesignation()]['cas']['unhospitalize'][$currentSuperArrayIndex]['F'];
                    $superArray[$soin->getDiagnostique()->getDesignation()]['cas']['unhospitalize'][$currentSuperArrayIndex]['F'] = $lastWomenNormalCaseCount + 1;
                }
            }


            }
        }
    }

}
