<?php

namespace App\Controller\AppBundle\Controller;

use StockBundle\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default_default")
     */
    public function indexAction(Request $request)
    {

        // liste des médicament périmé*
        $em = $this->getDoctrine()->getManager();
        $rendezVouses = $em->getRepository('AppBundle:RendezVous')->findBy(['estConsumer' => 0, 'estValider' => 1, "estAnnuler" => 0]);
        $rdvArry = [];
        foreach ($rendezVouses as $event) {
            $today = new \DateTime('now');
            $today_time = strtotime($today->format('Y-m-d'));
            $expire_time = strtotime($event->getDateDemande()->format('Y-m-d'));

            if ($expire_time < $today_time) {
                $event->setEstAnnuler(1);
                $em->persist($event);
            }
            $rdvArry[] = [
                "title" => $event->getPatient()->getFullname(),
                "start" => $event->getDateDemande()->format('Y-m-d'),
                "url" => $event->getId() . '/edit'
            ];
        }
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig',[
            'rdv' => json_encode($rdvArry),
        ]);
    }

    /**
     * @Route("/default_list", name="default_listingPage")
     */
    public function default_listingPage(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/default_new", name="default_new")
     */
    public function default_new(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/medicament_perime", name="medicamentPerime")
     */
    public function medicamentPerime(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $medicamentPerime  = $em->getRepository(Article::class)->findExpiredMedic();
        return $this->render('default/medicamentPerime.html.twig',[
            'medicamentPerime'=>$medicamentPerime
        ]);
    }

    /**
     * @Route("/article_seuil", name="articleSeuil")
     */
    public function articleSeuil(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $articleSeuil  = $em->getRepository(Article::class)->findArticleAlmostReachThreshold();
//        dump($articleSeuil);die();
        return $this->render('default/articleSeuil.html.twig',[
            'articleSeuil'=>$articleSeuil
        ]);
    }
}
