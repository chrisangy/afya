<?php

namespace App\Controller\AppBundle\Controller;

use AppBundle\Entity\TypeDonne;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Typedonne controller.
 *
 * @Route("typedonne")
 */
class TypeDonneController extends AbstractController
{


    /**
     * Lists all typeDonne entities.
     *
     * @Route("/", name="typedonne_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $typeDonnes = $em->getRepository('AppBundle:TypeDonne')->findBy([], ['id' => 'DESC']);

        return $this->render('typedonne/index.html.twig', array(
            'typeDonnes' => $typeDonnes,
        ));
    }

    /**
     * Creates a new typeDonne entity.
     *
     * @Route("/new", name="typedonne_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $typeDonne = new Typedonne();
        $form = $this->createForm('AppBundle\Form\TypeDonneType', $typeDonne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $serachifexist = $em->getRepository(TypeDonne::class)->findOneBy(['libelle' => $typeDonne->getLibelle()]);
            if ($serachifexist) {
                $message = "Un type de données portant le meme nom existe déjà.";
                $this->get('session')->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('typedonne_index');
            }
            $typeDonne
                ->setLibelle(strtoupper($typeDonne->getLibelle()))->setCreatedAt(new \DateTime())
                ->setDeleted(false);
            $em->persist($typeDonne);
            $em->flush();
            $message = "Données sauvegardées avec succès";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('typedonne_index');
        }
        return $this->render('typedonne/new.html.twig', array(
            'typeDonne' => $typeDonne,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing typeDonne entity.
     *
     * @Route("/{reference}/edit", name="typedonne_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TypeDonne $typeDonne)
    {
        $editForm = $this->createForm('AppBundle\Form\TypeDonneType', $typeDonne);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $typeDonne->setLibelle(strtoupper($typeDonne->getLibelle()));
            $this->getDoctrine()->getManager()->flush();
            $message = "Données modifier avec succès";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('typedonne_index');
        }

        return $this->render('typedonne/edit.html.twig', array(
            'typeDonne' => $typeDonne,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a typeDonne entity.
     *
     * @Route("/change/visibilite/{reference}", name="typedonne_delete")
     */
    public function deleteAction(Request $request, TypeDonne $typeDonne)
    {

        $em = $this->getDoctrine()->getManager();
        if ($typeDonne->getDeleted() === true) {
            $typeDonne->setDeleted(0);
        } else {
            $typeDonne->setDeleted(1);
        }
        $em->flush();
        $message = "La visibilité du type de données " . $typeDonne->getLibelle() . " a été modifiée avec succès.";
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('typedonne_index');
    }

    /**
     * Creates a form to delete a typeDonne entity.
     *
     * @param TypeDonne $typeDonne The typeDonne entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TypeDonne $typeDonne)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typedonne_delete', array('id' => $typeDonne->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
