<?php

namespace App\Controller\AppBundle\Controller;

use AppBundle\Entity\Consultation;
use AppBundle\Entity\ConsultationDonneSuivi;
use AppBundle\Entity\DonneSuivi;
use AppBundle\Entity\Dossier;
use AppBundle\Entity\TypeDonne;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Consultation controller.
 *
 * @Route("consultation")
 */
class ConsultationController extends AbstractController
{
    /**
     * Lists all consultation entities.
     *
     * @Route("/", name="consultation_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $consultations = $em->getRepository('AppBundle:Consultation')->findBy([], ['dateConsultation' => 'DESC']);

        return $this->render('consultation/index.html.twig', array(
            'consultations' => $consultations,
        ));
    }

    /**
     * Creates a new consultation entity.
     *
     * @Route("/new", name="consultation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $consultation = new Consultation();
        $form = $this->createForm('AppBundle\Form\ConsultationType', $consultation);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $donneSuivi = $request->get('donnesuivi');
            $dossierCode = $request->request->get('appbundle_consultation')['dossier'];
            $dossier = $em->getRepository(Dossier::class)->findOneBy(['code' => $dossierCode]);
            foreach ($donneSuivi as $donnes) {
                $consultationSuivi = new ConsultationDonneSuivi();
                $donnesuivi = $em->getRepository(DonneSuivi::class)->findOneBy(['reference' => $donnes]);
                $consultationSuivi->setConsultation($consultation)
                    ->setDonnesuivi($donnesuivi);
                $em->persist($consultationSuivi);
            }
            $consultation->setDossier($dossier);
            $em->persist($consultation);
            $em->flush();
            $this->addFlash("success", "Enregistrement éffectué avec succès");
            return $this->redirectToRoute('registresoins_new_redirect',[
                'matricule'=>$consultation->getDossier()->getPatient()->getMatricule()
            ]);
        }
        $typeSuivi = $em->getRepository(TypeDonne::class)->findBy(['parent'=>null,'deleted'=>false]);
        return $this->render('consultation/new.html.twig', array(
            'consultation' => $consultation,
            'form' => $form->createView(),
            'typeSuivi' => $typeSuivi
        ));
    }

    /**
     * Finds and displays a consultation entity.
     *
     * @Route("/detail/{reference}", name="consultation_show")
     * @Method("GET")
     */
    public function showAction(Consultation $consultation)
    {
        $em = $this->getDoctrine()->getManager();
        $donneSuivi = $em->getRepository(ConsultationDonneSuivi::class)->findBy(['consultation' => $consultation]);
        return $this->render('consultation/show.html.twig', array(
            'consultation' => $consultation,
            'donneSuivi'=>$donneSuivi
        ));
    }

    /**
     * Displays a form to edit an existing consultation entity.
     *
     * @Route("/{reference}/edit", name="consultation_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Consultation $consultation)
    {
        $deleteForm = $this->createDeleteForm($consultation);
        $editForm = $this->createForm('AppBundle\Form\ConsultationType', $consultation);
        $editForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $donnesuivi = $em->getRepository(ConsultationDonneSuivi::class)->findBy(['consultation' => $consultation]);
        // dump($donnesuivi);die();
        if ($editForm->isSubmitted()) {
            $donneSuivi = $request->get('donnesuivi');
            $dossierCode = $request->request->get('appbundle_consultation')['dossier'];
//            dump($dossierCode,$request);die();

            $dossier = $em->getRepository(Dossier::class)->find($dossierCode);
            $consultation->setDossier($dossier);
            foreach ($donneSuivi as $donnes) {
                $donnesuivi = $em->getRepository(DonneSuivi::class)->findOneBy(['reference' => $donnes]);
                $check = $em->getRepository(ConsultationDonneSuivi::class)->findOneBy(['consultation' => $consultation, 'donnesuivi' => $donnesuivi]);
                if ($check === null) {
                    $consultationSuivi = new ConsultationDonneSuivi();
                    $consultationSuivi->setConsultation($consultation)
                        ->setDonnesuivi($donnesuivi);
                    $em->persist($consultationSuivi);
                }
            }
            $em->flush();

            $this->addFlash("success", "Modification éffectuée avec succès");
            return $this->redirectToRoute('consultation_edit', array('reference' => $consultation->getReference()));
        }
        $typeSuivi = $em->getRepository(TypeDonne::class)->findBy(['parent'=>null,'deleted'=>false]);
        return $this->render('consultation/edit.html.twig', array(
            'consultation' => $consultation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'typeSuivi' => $typeSuivi,
            'donneSuivi' => $donnesuivi
        ));
    }

    /**
     * Deletes a consultation entity.
     *
     * @Route("/{id}", name="consultation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Consultation $consultation)
    {
        $form = $this->createDeleteForm($consultation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($consultation);
            $em->flush();
        }
        $this->addFlash("success", "Enregistrement effacé avec succès");
        return $this->redirectToRoute('consultation_index');
    }

    /**
     * Creates a form to delete a consultation entity.
     *
     * @param Consultation $consultation The consultation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Consultation $consultation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('consultation_delete', array('id' => $consultation->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
