<?php

namespace App\Controller\AppBundle\Controller;

use AppBundle\Entity\ConsultationDonneSuivi;
use AppBundle\Entity\DonneSuivi;
use AppBundle\Entity\TypeDonne;
use AppBundle\Form\Model\PeriodeSearchType;
use AppBundle\Model\PeriodSearch;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class StatistiqueController extends AbstractController
{
    /**
     * index of statistiques donne suivi
     *
     * @Route("/statitique/donne-suivi", name="statitique_soins_suivi")
     * @Method("POST")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
//        $periodSearch = new PeriodSearch();
//        $form = $this->createForm(PeriodeSearchType::class, $periodSearch);
        $searchresult = $donneSuivi = $tabledata = [];
//        $form->handleRequest($request);
        $typeSuivi = $em->getRepository(TypeDonne::class)->findBy(['parent' => null, 'deleted' => false]);
        $typeDonne = $sousdonnes = $datedebut = $datefin = '';
        if ($request->isMethod('POST')) {
            $typeDonne = $request->get('typedonne');
            $soustype = $request->get('soustype');
            $mois = $request->get('mois');
            $annees = $request->get('annees');
            $query_date = $annees . '-' . $mois . '-01';
            $datedebut = new \DateTime(date('Y-m-01', strtotime($query_date)));
            $datefin = new \DateTime(date('Y-m-t', strtotime($query_date)));
            $estEnfant = true;
            $typedonnes = $em->getRepository(TypeDonne::class)->findOneBy(['reference' => $typeDonne, 'deleted' => false, 'parent' => null]);
            if ($soustype) {

                $typedonnes = $em->getRepository(TypeDonne::class)->findOneBy(['reference' => $soustype, 'deleted' => false]);
                $donneSuivi = $em->getRepository(DonneSuivi::class)->findBy(['subtypeDonne' => $typedonnes, 'deleted' => false]);
            } else {
                $soustype = $typeDonne;
                $donneSuivi = $em->getRepository(DonneSuivi::class)->findBy(['typeDonne' => $typedonnes, 'subtypeDonne' => null, 'deleted' => false]);
                $estEnfant = false;
            }

            $searchresult = $em->getRepository(ConsultationDonneSuivi::class)->dataByPeriod($soustype, $datedebut, $datefin, $estEnfant);
//dump($searchresult,$datedebut,$datefin);die();
            return $this->render('donnesuivi/impression.html.twig',
                [
                    'typeDonneParent' => $typeDonne,
                    'typesuivi' => $typeSuivi,
                    'table' => $searchresult,
                    'donneSuivi' => $donneSuivi,
                    'mois' => $mois,
                    'annees' => $annees,
                    'typedonne' => $soustype,
                     'donnes' => $typedonnes,
                    'datedebut' => $datedebut->format('Y-m-d'),
                    'datefin' => $datefin->format('Y-m-d')
                ]);
        }
        return $this->render('donnesuivi/impression.html.twig',
            [
                'typesuivi' => $typeSuivi,
                'table' => $searchresult,
                'donneSuivi' => $donneSuivi,
                'typedonne' => $sousdonnes,
                'datedebut' => $datedebut,
                'datefin' => $datefin
            ]);
    }

    /**
     * impression of statistiques donne suivi
     *
     * @Route("/imprimer/statitique/donne-suivi/{typedonneParent}/{typedonne}/{datedebut}/{datefin}", name="impression_statitique_soins_suivi")
     * @Method("GET")
     */
    public function imprimerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $typedonneParent = $request->get('typedonneParent');
        $typeDonne = $request->get('typedonne');
        $datedebut = $request->get('datedebut');
        $datefin = $request->get('datefin');
        $typedonneParents = $em->getRepository(TypeDonne::class)->findOneBy(['reference' => $typedonneParent]);
        $tydonnes = $em->getRepository(TypeDonne::class)->findOneBy(['reference' => $typeDonne]);
        $donneSuivi = $em->getRepository(DonneSuivi::class)->findBy(['subtypeDonne' => $tydonnes]);
        $donneSuivis= $this->group_arra_by_key('libelleSubtype',$em->getRepository(DonneSuivi::class)->findByTypeDonne($typedonneParents));
    //    dump($donneSuivis);die();

        $searchresult = $em->getRepository(ConsultationDonneSuivi::class)->dataByPeriod($typeDonne, new \DateTime($datedebut), new \DateTime($datefin));
    //    dump($searchresult);die();
        if ($tydonnes->getLibelle() === 'INDICATEUR' || ($tydonnes->getParent() !== null && $tydonnes->getParent()->getLibelle() === 'INDICATEUR') ) {
            $etat = 'etat/indicateurPdf.html.twig';
        }
        if ($tydonnes->getLibelle() === 'VARIABLES' || ($tydonnes->getParent() !== null && $tydonnes->getParent()->getLibelle() === 'VARIABLES') ) {
            $etat = 'etat/variablesPdf.html.twig';
        }
        if ($tydonnes->getLibelle() === 'ACTIVITES DE MATERNITE' || ($tydonnes->getParent() !== null && $tydonnes->getParent()->getLibelle() === 'ACTIVITES DE MATERNITE') ) {
            $etat = 'etat/activiteMaternitePdf.html.twig';
        }
        if ($tydonnes->getLibelle() === 'PTME' || ($tydonnes->getParent() !== null && $tydonnes->getParent()->getLibelle() === 'PTME') ) {
            $etat = 'etat/ptmePdf.html.twig';
        }
        // dump($donneSuivis,$searchresult);die();
    //    return $this->render($etat, [
    //        'tabledata' => $searchresult,
    //        'donneSuivi' => $donneSuivis,
    //        'datedebut' => $datedebut,
    //        'datefin' => $datefin,

    //    ]);
        $html = $this->renderView($etat, [
            'tabledata' => $searchresult,
            'donneSuivi' => $donneSuivis,
            'datedebut' => $datedebut,
            'datefin' => $datefin,

        ]);
        $today = new \DateTime();
        $nomFIcher = strtoupper($tydonnes->getLibelle()) . '-' . $today->format('YmdHis').'.pdf';
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, []),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'orientation' => 'Landscape',
                'Content-Disposition' => 'attachment; filename= ' . $nomFIcher
            )
        );
    }

    function group_arra_by_key($key, $data)
    {
        $result = array();

        foreach ($data as $val) {
            if (array_key_exists($key, $val)) {
                $result[$val[$key]][] = $val;
            } else {
                $result[""][] = $val;
            }
        }
        return $result;
    }
}
