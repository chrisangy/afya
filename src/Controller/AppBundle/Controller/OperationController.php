<?php

namespace App\Controller\AppBundle\Controller;

use AppBundle\StaticClass\MECEFConstants;
use AppBundle\Entity\CompteDebiteur;
use AppBundle\Entity\Dettes;
use AppBundle\Entity\Operation;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use StockBundle\Entity\Article;
use StockBundle\Entity\CategorieArticle;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\Patient;
use UserBundle\Entity\Personne;

/**
 * Operation controller.
 *
 * @Route("operation")
 */
class OperationController extends AbstractController
{
    /**
     * Lists all operation entities.
     *
     * @Route("/", name="operation_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $this->redirectToRoute('default_default');
        $em = $this->getDoctrine()->getManager();

        $operations = $em->getRepository('AppBundle:Operation')->findBy([],['id'=>'DESC']);

        return $this->render('operation/index.html.twig', array(
            'operations' => $operations,
        ));
    }

    /**
     * Creates a new operation entity.
     *
     * @Route("/new", name="operation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {

        $this->redirectToRoute('default_default');
        $operation = new Operation();

        $form = $this->createForm('AppBundle\Form\OperationType', $operation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $montantRemisParClient = $request->request->get('appbundle_operation')['montantRemisParClient'];
            $operations = json_decode($request->get('operation_data_container'));;
            $user = $this->getUser();
            $reference = uniqid('', true);
            $montatTotalOperation = 0;
            $montantTotalDette = $total=0;
            // le patient a qui on fait l'opération
            $compteDette = $em->getRepository(CompteDebiteur::class)->find($operations[0]->appbundle_operation_comptedette);
            $article=[];
            foreach ($operations as $currentOperation) {

                $produit = $em->getRepository(Article::class)->find($currentOperation->appbundle_operation_produit);

                if ($currentOperation->appbundle_operation_estdette == true) {
                    $montantTotalDette += $produit->getPrixVente();
                }
                $montatTotalOperation += $produit->getPrixVente();
                $article[] = [
                    'name' => $produit->getDesignation(),
                    'price' => (int)$produit->getPrixVente(),
                    'quantity' => $currentOperation->appbundle_operation_nombre,
                    'taxGroup' => 'A',
                ];
                $total += (int)$currentOperation->appbundle_operation_nombre * (int)$produit->getPrixVente();
            }
            /*Generate EMEFEC FACTURE and VALIDATE IT*/
            $url = MECEFConstants::API_INFORMATION_URL . 'api/invoice';
            $responseCodeFacutre = json_decode($this->get('mecef_service')->mecefinit($url, $article, $total,$user));
            if (!is_null($responseCodeFacutre)) {
                $url = MECEFConstants::API_INFORMATION_URL . 'api/invoice/' . $responseCodeFacutre->uid . '/confirm';
                $donnemefec = json_decode($this->get('mecef_service')->mecefConirm($url));
            }

            foreach ($operations as $currentOperation) {

                $operation = new Operation();
                $patient = $em->getRepository(Personne::class)->find($currentOperation->appbundle_operation_patient);
                $produit= $em->getRepository(Article::class)->find($currentOperation->appbundle_operation_produit);
//                Save E-MECEF INFO
                $operation->setNimMECeF($donnemefec->nim)
                    ->setCodeMECeFDGI($donnemefec->codeMECeFDGI)
                    ->setCountersMECeF($donnemefec->counters)
                    ->setQrCodeMECeFDGI($donnemefec->qrCode)
                    ;

                $operation->setDateOperation(new \DateTime($currentOperation->appbundle_operation_dateoperation));
                $operation->setPatient($patient);
//                $operation->setCategorieArticle($categorie);
                $operation->setReference($reference)
                ->setPrix($produit->getPrixVente());
                $operation->setProduit($produit);
                $operation->setMontantRemisParClient((int)$montantRemisParClient);
                $operation->setMonnaieRendu($operation->getMontantRemisParClient() - $montatTotalOperation);
                $operation->setMontant($produit->getPrixVente() * $currentOperation->appbundle_operation_nombre);
                $operation->setNombre($currentOperation->appbundle_operation_nombre);


                $operation->setLibelleOperation(' ');
                $operation->setUser($user);
                $em->persist($operation);

            }

            /**
             * @todo  elever quand la partie dette sera reglé
             */
            $this->enregistrerDette($compteDette, $montantTotalDette);
            $em->flush();

            $this->addFlash("success", "Enregistrement effectuée avec succès");
            return $this->redirectToRoute('operation_show', array('reference' => $reference));
        }

        END:
        return $this->render('operation/new.html.twig', array(
            'operation' => $operation,
            'form' => $form->createView(),
        ));
    }


    /**
     * Finds and displays a operation entity.
     *
     * @Route("/recu/client/{reference}", name="recu_operation_client")
     * @Method("GET")
     */
    public function generateRecu($reference)
    {
        $this->redirectToRoute('default_default');

        $em = $this->getDoctrine()->getManager();
        $operations = $em->getRepository('AppBundle:Operation')->findBy(['reference' => $reference]);
//dump($operations);die();
        $html = $this->renderView('operation/pdf/recu.html.twig', array(
//            'donnemefec' => json_decode($donnemefec),
            'operations' => $operations
        ));

        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html/*[

                'page-height' => 420,
                'page-width'  => 595,
                'encoding' => 'utf-8',
                'images' => true,
                'dpi' => 72
            ]*/),
            'Recu operation_' . $reference . '.pdf'
        );

    }

    /**
     * Finds and displays a operation entity.
     *
     * @Route("/{reference}", name="operation_show")
     * @Method("GET")
     */
    public
    function showAction($reference)
    {
        $this->redirectToRoute('default_default');

//        $deleteForm = $this->createDeleteForm($operation);
        $em = $this->getDoctrine()->getManager();
        $operations = $em->getRepository('AppBundle:Operation')->findBy(['reference' => $reference]);


        return $this->render('operation/show.html.twig', array(
            'operations' => $operations,
        ));
    }

    /**
     * Creates a form to delete a operation entity.
     *
     * @param Operation $operation The operation entity
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private
    function createDeleteForm(Operation $operation)
    {
        $this->redirectToRoute('default_default');
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('operation_delete', array('id' => $operation->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a form to edit an existing operation entity.
     *
     * @Route("/{id}/edit", name="operation_edit")
     * @Method({"GET", "POST"})
     */
    public
    function editAction(Request $request, Operation $operation)
    {
        $this->redirectToRoute('default_default');
        $deleteForm = $this->createDeleteForm($operation);
        $editForm = $this->createForm('AppBundle\Form\OperationType', $operation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash("success", "Modification éffectuée avec succès");
            return $this->redirectToRoute('operation_edit', array('id' => $operation->getId()));
        }

        return $this->render('operation/edit.html.twig', array(
            'operation' => $operation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a operation entity.
     *
     * @Route("/{id}", name="operation_delete")
     * @Method("DELETE")
     */
    public
    function deleteAction(Request $request, Operation $operation)
    {
        $this->redirectToRoute('default_default');
        $form = $this->createDeleteForm($operation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($operation);
            $em->flush();
        }

        return $this->redirectToRoute('operation_index');
    }


    private
    function enregistrerDette($personne, $montantDette)
    {
        $this->redirectToRoute('default_default');

        if ($montantDette > 0) {
            $em = $this->getDoctrine()->getManager();
            /**
             * @var Dettes $dettePatient
             */
            $dettePatient = $em->getRepository(Dettes::class)->findOneByCompte($personne->getId());
            if (!$dettePatient) {
                $dettePatient = (new Dettes())
                    ->setMontant($montantDette)
                    ->setCompte($personne);
            } else {
                $dettePatient->setMontant($dettePatient->getMontant() + $montantDette);
            }

            $em->persist($dettePatient);

        }


    }
}
