<?php

namespace App\Controller\AppBundle\Controller;

use AppBundle\Entity\DonneSuivi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Donnesuivi controller.
 *
 * @Route("donnesuivi")
 */
class DonneSuiviController extends AbstractController
{
    /**
     * Lists all donneSuivi entities.
     *
     * @Route("/", name="donnesuivi_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $donneSuivis = $em->getRepository('AppBundle:DonneSuivi')->findBy(['deleted'=>0],['id'=>'DESC']);

        return $this->render('donnesuivi/index.html.twig', array(
            'donneSuivis' => $donneSuivis,
        ));
    }

    /**
     * Creates a new donneSuivi entity.
     *
     * @Route("/new", name="donnesuivi_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $donneSuivi = new Donnesuivi();
        $form = $this->createForm('AppBundle\Form\DonneSuiviType', $donneSuivi);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($em->getRepository(Donnesuivi::class)->findOneBy(['libelle' => $donneSuivi->getLibelle(),'typeDonne'=>$donneSuivi->getTypeDonne(),'subtypeDonne'=>$donneSuivi->getSubtypeDonne()])) {
                $message = "Un type de données portant le meme nom existe déjà.";
                $this->get('session')->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('donnesuivi_index');
            }
            $donneSuivi->setLibelle(strtoupper($donneSuivi->getLibelle()));
            $donneSuivi->setCreatedAt(new \DateTime())
                ->setDeleted(false);
            $em->persist($donneSuivi);
            $em->flush();
            $message = "Données enregistré avec succès";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('donnesuivi_index');
        }

        return $this->render('donnesuivi/new.html.twig', array(
            'donneSuivi' => $donneSuivi,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing donneSuivi entity.
     *
     * @Route("/{reference}/edit", name="donnesuivi_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, DonneSuivi $donneSuivi)
    {
        $em = $this->getDoctrine()->getManager();
        $editForm = $this->createForm('AppBundle\Form\DonneSuiviType', $donneSuivi);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if ($em->getRepository(Donnesuivi::class)->findOneBy(['libelle' => strtoupper($donneSuivi->getLibelle()),'typeDonne'=>$donneSuivi->getTypeDonne(),'subtypeDonne'=>$donneSuivi->getSubtypeDonne()])) {
                $message = "Un type de données portant le meme nom existe déjà.";
                $this->get('session')->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('donnesuivi_index');
            }
            $donneSuivi->setLibelle(strtoupper($donneSuivi->getLibelle()));
            $this->getDoctrine()->getManager()->flush();
            $message = "Données modifié avec succès";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('donnesuivi_index');
        }

        return $this->render('donnesuivi/edit.html.twig', array(
            'donneSuivi' => $donneSuivi,
            'form' => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a donneSuivi entity.
     *
     * @Route("/change/visibilite/{reference}", name="donnesuivi_delete")
     */
    public function deleteAction(Request $request, DonneSuivi $donneSuivi)
    {

        $em = $this->getDoctrine()->getManager();
        if ($donneSuivi->getDeleted() === true) {
            $donneSuivi->setDeleted(0);
        } else {
            $donneSuivi->setDeleted(1);
        }
        $em->flush();
        $message = "La visibilité du type de données " . $donneSuivi->getLibelle() . " a été modifiée avec succès.";
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('donnesuivi_index');
    }

    /**
     * Creates a form to delete a donneSuivi entity.
     *
     * @param DonneSuivi $donneSuivi The donneSuivi entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(DonneSuivi $donneSuivi)
    {
//        return $this->createFormBuilder()
//            ->setAction($this->generateUrl('donnesuivi_delete', array('id' => $donneSuivi->getId())))
//            ->setMethod('DELETE')
//            ->getForm();
    }
}
