<?php

namespace App\Controller\AppBundle\Controller;

use AppBundle\Entity\SchemaOperation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Schemaoperation controller.
 *
 * @Route("schemaoperation")
 */
class SchemaOperationController extends AbstractController
{
    /**
     * Lists all schemaOperation entities.
     *
     * @Route("/", name="schemaoperation_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $schemaOperations = $em->getRepository('AppBundle:SchemaOperation')->findAll();

        return $this->render('schemaoperation/index.html.twig', array(
            'schemaOperations' => $schemaOperations,
        ));
    }

    /**
     * Creates a new schemaOperation entity.
     *
     * @Route("/new", name="schemaoperation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $schemaOperation = new Schemaoperation();
        $form = $this->createForm('AppBundle\Form\SchemaOperationType', $schemaOperation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($schemaOperation);
            $em->flush();

            return $this->redirectToRoute('schemaoperation_show', array('id' => $schemaOperation->getId()));
        }

        return $this->render('schemaoperation/new.html.twig', array(
            'schemaOperation' => $schemaOperation,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a schemaOperation entity.
     *
     * @Route("/{id}", name="schemaoperation_show")
     * @Method("GET")
     */
    public function showAction(SchemaOperation $schemaOperation)
    {
        $deleteForm = $this->createDeleteForm($schemaOperation);

        return $this->render('schemaoperation/show.html.twig', array(
            'schemaOperation' => $schemaOperation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing schemaOperation entity.
     *
     * @Route("/{id}/edit", name="schemaoperation_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, SchemaOperation $schemaOperation)
    {
        $deleteForm = $this->createDeleteForm($schemaOperation);
        $editForm = $this->createForm('AppBundle\Form\SchemaOperationType', $schemaOperation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('schemaoperation_edit', array('id' => $schemaOperation->getId()));
        }

        return $this->render('schemaoperation/edit.html.twig', array(
            'schemaOperation' => $schemaOperation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a schemaOperation entity.
     *
     * @Route("/{id}", name="schemaoperation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, SchemaOperation $schemaOperation)
    {
        $form = $this->createDeleteForm($schemaOperation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($schemaOperation);
            $em->flush();
        }

        return $this->redirectToRoute('schemaoperation_index');
    }

    /**
     * Creates a form to delete a schemaOperation entity.
     *
     * @param SchemaOperation $schemaOperation The schemaOperation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SchemaOperation $schemaOperation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('schemaoperation_delete', array('id' => $schemaOperation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
