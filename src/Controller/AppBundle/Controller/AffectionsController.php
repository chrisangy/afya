<?php

namespace App\Controller\AppBundle\Controller;

use AppBundle\Entity\Affections;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Affection controller.
 *
 * @Route("affections")
 */
class AffectionsController extends AbstractController
{
    /**
     * Lists all affection entities.
     *
     * @Route("/", name="affections_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $affections = $em->getRepository('AppBundle:Affections')->findAll();

        return $this->render('affections/index.html.twig', array(
            'affections' => $affections,
        ));
    }

    /**
     * Creates a new affection entity.
     *
     * @Route("/new", name="affections_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $affection = new Affections();
        $form = $this->createForm('AppBundle\Form\AffectionsType', $affection);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($affection);
            $em->flush();

            return $this->redirectToRoute('affections_new');
//            return $this->redirectToRoute('affections_show', array('id' => $affection->getId()));
        }

        return $this->render('affections/new.html.twig', array(
            'affection' => $affection,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a affection entity.
     *
     * @Route("/{id}", name="affections_show")
     * @Method("GET")
     */
    public function showAction(Affections $affection)
    {
        $deleteForm = $this->createDeleteForm($affection);

        return $this->render('affections/show.html.twig', array(
            'affection' => $affection,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing affection entity.
     *
     * @Route("/{id}/edit", name="affections_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Affections $affection)
    {
        $deleteForm = $this->createDeleteForm($affection);
        $editForm = $this->createForm('AppBundle\Form\AffectionsType', $affection);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('affections_edit', array('id' => $affection->getId()));
        }

        return $this->render('affections/edit.html.twig', array(
            'affection' => $affection,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a affection entity.
     *
     * @Route("/{id}", name="affections_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Affections $affection)
    {
        $form = $this->createDeleteForm($affection);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($affection);
            $em->flush();
        }

        return $this->redirectToRoute('affections_index');
    }

    /**
     * Creates a form to delete a affection entity.
     *
     * @param Affections $affection The affection entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Affections $affection)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('affections_delete', array('id' => $affection->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
