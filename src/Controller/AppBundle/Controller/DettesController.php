<?php

namespace App\Controller\AppBundle\Controller;

use AppBundle\Entity\Dettes;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Dette controller.
 *
 * @Route("dettes")
 */
class DettesController extends AbstractController
{
    /**
     * Lists all dette entities.
     *
     * @Route("/", name="dettes_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $dettes = $em->getRepository('AppBundle:Dettes')->findAll();

        return $this->render('dettes/index.html.twig', array(
            'dettes' => $dettes,
        ));
    }

    /**
     * Creates a new dette entity.
     *
     * @Route("/new", name="dettes_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $dette = new Dette();
        $form = $this->createForm('AppBundle\Form\DettesType', $dette);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dette);
            $em->flush();

            return $this->redirectToRoute('dettes_show', array('id' => $dette->getId()));
        }

        return $this->render('dettes/new.html.twig', array(
            'dette' => $dette,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a dette entity.
     *
     * @Route("/{id}", name="dettes_show")
     * @Method("GET")
     */
    public function showAction(Dettes $dette)
    {
        $deleteForm = $this->createDeleteForm($dette);

        return $this->render('dettes/show.html.twig', array(
            'dette' => $dette,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing dette entity.
     *
     * @Route("/{id}/edit", name="dettes_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Dettes $dette)
    {
        $deleteForm = $this->createDeleteForm($dette);
        $editForm = $this->createForm('AppBundle\Form\DettesType', $dette);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dettes_edit', array('id' => $dette->getId()));
        }

        return $this->render('dettes/edit.html.twig', array(
            'dette' => $dette,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a dette entity.
     *
     * @Route("/{id}", name="dettes_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Dettes $dette)
    {
        $form = $this->createDeleteForm($dette);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($dette);
            $em->flush();
        }

        return $this->redirectToRoute('dettes_index');
    }

    /**
     * Creates a form to delete a dette entity.
     *
     * @param Dettes $dette The dette entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Dettes $dette)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('dettes_delete', array('id' => $dette->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
