<?php

namespace App\Repository\AppBundle;

use App\Entity\AppBundle\OrdonanceMedicament;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * OrdonanceMedicamentRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class OrdonanceMedicamentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, OrdonanceMedicament::class);
    }
}
