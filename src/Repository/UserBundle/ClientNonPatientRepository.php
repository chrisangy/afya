<?php

namespace App\Repository\UserBundle;

use App\Entity\UserBundle\ClientNonPatient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * ClientNonPatientRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ClientNonPatientRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ClientNonPatient::class);
    }
    public function findClientNonPatient()
    {
        return $this->createQueryBuilder('a')
            ->andWhere("a.estPatient= :p1")
            ->setParameter("p1", false)
            ->orderBy('a.nom', 'ASC')
            ->getQuery()
            ->getResult();
    }

}
