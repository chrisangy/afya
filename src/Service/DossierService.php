<?php
/**
 * Created by PhpStorm.
 * User: LANGANFIN  Rogelio
 * Date: 05/09/2020
 * Time: 10:38
 */

namespace App\Service;


use App\Entity\AppBundle\Dossier;
use Symfony\Component\DependencyInjection\Container;
use App\Entity\UserBundle\Patient;

class DossierService
{

    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }


    public function createDossier(Patient $patient,$observation)
    {

        $reference = uniqid();

        try {
            $dossier = (new Dossier())
                ->setNom($patient->getNom())
                ->setPrenom($patient->getPrenom())
                ->setPatient($patient)
                ->setCode($reference)
                ->setNote($observation);

            $em = $this->container->get('doctrine')->getManager();

            $em->persist($dossier);
//            $em->flush();

            return $dossier;
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

    }

}