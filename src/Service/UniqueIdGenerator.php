<?php
/**
 * Created by PhpStorm.
 * User: LANGANFIN Rogelio
 * Date: 30/12/2019
 * Time: 11:51
 */

namespace AppBundle\Service;


use Symfony\Component\DependencyInjection\Container;

use Ramsey\Uuid\Uuid;

class UniqueIdGenerator
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function uniqueId()
    {
        try {
            // Generate a version 1 (time-based) UUID object
            $uuid1 = Uuid::uuid1();
            return $uuid1;
//            // e.g ba51070a-d754-11e7-b225-4ccc6ab413a6
//            echo $uuid1->toString() . "<br>";
//
//            // Generate a version 3 (name-based and hashed with MD5) UUID object
//            $uuid3 = Uuid::uuid3(Uuid::NAMESPACE_DNS, 'php.net');
//
//            // e.g 11a38b9a-b3da-360f-9353-a5a725514269
//            echo $uuid3->toString() . "<br>";
//
//            // Generate a version 4 (random) UUID object
//            $uuid4 = Uuid::uuid4();
//
//            // e.g 0e9139c5-8e06-4a1a-bb5e-52a0abcd0072
//            echo $uuid4->toString() . "<br>";
//
//            // Generate a version 5 (name-based and hashed with SHA1) UUID object
//            $uuid5 = Uuid::uuid5(Uuid::NAMESPACE_DNS, 'php.net');
//
//            // e.g c4a760a8-dbcf-5254-a0d9-6a4474bd1b62
//            echo $uuid5->toString() . "<br>";

            return new Response();
        } catch (UnsatisfiedDependencyException $e) {
            // Some dependency was not met. Either the method cannot be called on a
            // 32-bit system, or it can, but it relies on Moontoast\Math to be present.
            throw new HttpException(500, 'Caught exception: ' . $e->getMessage());
        }
        
    }

}