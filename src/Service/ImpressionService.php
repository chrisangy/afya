<?php
/**
 * Created by PhpStorm.
 * User: LANGANFIN  Rogelio
 * Date: 28/02/2021
 * Time: 02:32
 */

namespace App\Service;


use Symfony\Component\DependencyInjection\Container;

class ImpressionService
{

    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }



    public function createUsageArray(&$arrayModified)
    {
        $detailData = ['M' => 0, 'F' => 0];
        if (!isset($arrayModified['01'])){
            $arrayModified['01'] =$detailData;
        }
        if (!isset($arrayModified['111'])){
            $arrayModified['111'] =$detailData;
        }
        if (!isset($arrayModified['14'])){
            $arrayModified['14'] =$detailData;
        }
        if (!isset($arrayModified['59'])){
            $arrayModified['59'] =$detailData;
        }
        if (!isset($arrayModified['1014'])){
            $arrayModified['1014'] =$detailData;
        }
        if (!isset($arrayModified['1519'])){
            $arrayModified['1519'] =$detailData;
        }
        if (!isset($arrayModified['2024'])){
            $arrayModified['2024'] =$detailData;
        }
        if (!isset($arrayModified['25'])){
            $arrayModified['25'] =$detailData;
        }


    }
}