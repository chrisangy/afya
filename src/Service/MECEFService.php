<?php

namespace AppBundle\Service;

use AppBundle\StaticClass\MECEFConstants;

class MECEFService
{

    public function mecefinit($url, $items, $total,$vendeur)
    {
        try {
            $content = [
                "ifu" => MECEFConstants::IFU,
                "type" => "FV",
                "items" => $items,
                "client" => [
                    "contact" => "97475882",
                    "ifu" => "3202112658883",
                    "name" => "CLINQUE ASSALAM",
                    "address" => "AGBLANGANDAN"
                ],
                "operator" => [
                    "id" => $vendeur->getPersonne()->getMatricule(),
                    "name" => $vendeur->getPersonne()->getNom().' '.$vendeur->getPersonne()->getPrenom()
                ],
                "payment" => [
                    [
                        "name" => "ESPECES",
                        "amount" => $total
                    ]
                ]
            ];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Cache-Control: no-cache",
                "content-type:application/json;charset=utf-8",
                "Authorization:" . MECEFConstants::JETON,
            ));
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($content));
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            if (curl_errno($ch)) {
                echo curl_error($ch);
                die();
            }
            //return new JsonResponse($response);
            $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($http_code == 200) {
                return $response;
            } else {
                dump("Error code : " . $http_code);
                die();
            }

        } catch (Exception $e) {
            echo 'Exception reçue : ', $e->getMessage(), "\n";
        } finally {
            curl_close($ch);

        }
    }

    public function mecefConirm($url)
    {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_PUT, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Cache-Control: no-cache",
                "content-type:application/json;charset=utf-8",
                "Authorization:" . MECEFConstants::JETON,
            ));
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            if (curl_errno($ch)) {
                echo curl_error($ch);
                die();
            }
            //return new JsonResponse($response);
            $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($http_code == 200) {
                return $response;
            } else {
                dump("Error code : " . $http_code);
                die();
            }

        } catch (Exception $e) {
            echo 'Exception reçue : ', $e->getMessage(), "\n";
        } finally {
            curl_close($ch);

        }
    }

}